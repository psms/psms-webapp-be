# We need to monkey_patch everything
from gevent import monkey
monkey.patch_all()

from flask import Flask
from flask_cors import CORS

from gevent import pywsgi

import random
import string
import views.index as index
import views.parts as parts
import views.categories as categories
import views.user as user
import views.diststatus as diststatus
import views.batches as batches
import views.devices as devices
import views.orders as orders
import views.tests as tests

import os

#Initialise FLASK Application
application = Flask(__name__)

try:
    application.secret_key = os.environ['SECRET_KEY']
except:
    application.secret_key = ''.join(random.choice(string.ascii_letters + string.digits) for i in range (20)) #DEBUG ONLY - to not be used on multi-worker env.

cors = CORS(application, support_credentials=True)
application.config['CORS_HEADERS'] = 'Content-Type'

indexview = index.IndexView(application)
partsview = parts.PartsView(application)
catview = categories.CategoriesView(application)
usrview = user.UserView(application)
diststatusview = diststatus.DistStatusView(application)
batchesview = batches.BatchesView(application)
devicesview = devices.DevicesView(application)
ordersview = orders.OrdersView(application)
testsview = tests.TestView(application)

if __name__ == "__main__":
    server = pywsgi.WSGIServer(('0.0.0.0', 5000), application)
    print('Start webserver..')
    server.serve_forever()
