# We need to monkey_patch everything
from flask import request
from flask import session
from flask import Response
from flask import make_response
from flask_cors import cross_origin

import math
import json
import os
import lib.database as DB

class PARTException(Exception):
    pass

class PartsView():
    def __init__(self, app):
        self.application = app
        app.add_url_rule('/restapi/get_parts', 'get_parts', self.get_parts, methods=['GET'])
        #TODO: Add get_parts_with_filters

        app.add_url_rule('/restapi/set_part', 'set_part', self.set_part, methods=['POST'])
        #app.add_url_rule('/restapi/get_part/<id>', 'get_part', self.get_part, methods=['GET'])

        self.connection_details = {
            'DB_HOST': os.environ['DB_HOST'],
            'DB_USER': os.environ['DB_USER'],
            'DB_PASS': os.environ['DB_PASS'],
            'DB_PORT': os.environ['DB_PORT'],
            'DB_DBNAME': os.environ['DB_DBNAME']
        }

    @cross_origin(supports_credentials=True)
    def get_parts(self):
        r = DB.db_get(
            '''
                SELECT
                    Parts.part_id                  as part_id,
                    Parts.name                     as name,
                    Parts.description              as description,
                    Parts.available                as available,
                    Parts.details                  as details,
                    Parts.unit_prices              as unit_prices,
                    Parts.user_inputs              as user_inputs,
                    Parts.batch_req_details        as batch_req_details,
                    Parts.purchase_status          as purchase_status,
                    Parts.reqshipment              as reqshipment,
                    Parts.moq                      as moq,
                    Categories.category_id         as category_id,
                    Categories.name                as category_name,
                    Categories.description         as category_description,
                    Categories.mandatory_details   as category_mandatory_details,
                    Users.user_id                  as user_id,
                    Users.email_addr               as user_email_addr,
                    Users.details                  as user_details
                FROM Parts
                LEFT JOIN PartCatLink ON PartCatLink.part_id = Parts.part_id
                LEFT JOIN Categories ON Categories.category_id = PartCatLink.category_id
                LEFT JOIN PartAdminLink ON PartAdminLink.part_id = Parts.part_id
                LEFT JOIN Users ON Users.user_id = PartAdminLink.user_id
            ''', [], connection_details=self.connection_details)

        grp = r.groupby('part_id')
        parts = {}

        for id, df in grp:
            part = {'categories':[], 'admins': []}
            rows = df.to_dict(orient='record')

            catids = []
            adminids = []

            for row in rows:
                part['part_id'] = row['part_id']
                part['name'] = row['name']
                part['description'] = row['description']
                part['available'] = True if row['available'] == 't' else False
                part['reqshipment'] = True if row['reqshipment'] == 't' else False
                part['details'] = json.loads(row['details'])
                part['moq'] = row['moq']
                part['user_inputs'] = json.loads(row['user_inputs'])
                part['unit_prices'] = json.loads(row['unit_prices'])
                part['batch_req_details'] = json.loads(row['batch_req_details'])
                part['purchase_status'] = [] #json.loads(row['purchase_status']) -- JM: Not supported yet

                try:
                    isempty = math.isnan(row['category_name'])
                except:
                    isempty = False

                if not isempty and row['category_id'] not in catids:
                    catids.append(row['category_id'])
                    part['categories'].append({
                        'id': row['category_id'],
                        'name': row['category_name'],
                        'description': row['category_description'],
                        'mandatory_details': json.loads(row['category_mandatory_details'])
                    })

                try:
                    isempty = math.isnan(row['user_id'])
                except:
                    isempty = False

                if not isempty and row['user_id'] not in adminids:
                    adminids.append(row['user_id'])

                    try:
                        username = json.loads(row['user_details'])['username']
                    except:
                        username = 'unknown'

                    part['admins'].append({
                        'email': row['user_email_addr'],
                        'username': username
                    })

            parts[str(row['part_id'])+'_'+row['name'].replace(' ','_')] = part

        return json.dumps({
            'application': 'CERN-DMS-BackEnd',
            'action': 'get_parts',
            'version': '0.0.1',
            'status': 0,
            'parts': parts
        })

    @cross_origin(supports_credentials=True)
    def set_part(self):

        '''
            Parts table:
            ============

            CREATE TABLE Parts(
                part_id             SERIAL          PRIMARY KEY ,
                name                VARCHAR(256)                ,
                description         TEXT                        ,
                available           BOOLEAN                     ,
                reqshipment         BOOLEAN                     ,
                moq                 BOOLEAN                     ,
                details             JSONB                       ,
                unit_prices         JSONB                       ,
                user_inputs         JSONB                       ,
                batch_req_details   JSONB                       ,
                purchase_status     JSONB
            );
        '''

        data = request.json
        errors = []

        if data['name'] == '':
            errors.append('Part name has to be set')

        if data['description'] == '':
            errors.append('Part description has to be set')

        formated_prices = []

        for p in data['prices']:
            if 'qty' not in p or 'chf' not in p:
                raise PARTException('Error: prices JSON is wrongly formated')

            try:
                formated_prices.append({'qty': int(p['qty']), 'chf': float(p['chf'])})
            except:
                errors.append('Price wrongly formated: "{}" - "{}" (shall be numbers)'.format(p['qty'], p['chf']))

        if len(data['prices']) <= 0:
            errors.append('Default price has to be set')

        if len(errors) > 0:
            return json.dumps({'status': -1, 'errors': errors})

        if int(data['id']) == -1:

            sql_request = 'INSERT INTO Parts(name, description, available, reqshipment, moq, details, unit_prices, user_inputs, batch_req_details, purchase_status) VALUES(%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)'
            sql_params = [
                data['name'],
                data['description'],
                bool(data['available']),
                bool(data['reqshipment']),
                int(data['moq']),
                json.dumps(data['details']),
                formated_prices,
                json.dumps(data['userinputs']),
                json.dumps(data['batch_req_details']),
                '{}' #json.dumps(data['purchasestatus']) --JM: Not supported yet
            ]

            DB.db_set(sql_request, sql_params, connection_details=self.connection_details)

        else:
            sql_request = 'UPDATE Parts SET name = %s, description = %s, available = %s, reqshipment = %s, moq = %s, details = %s, unit_prices = %s, user_inputs = %s, batch_req_details = %s, purchase_status = %s WHERE part_id = %s'
            sql_params = [
                data['name'],
                data['description'],
                bool(data['available']),
                bool(data['reqshipment']),
                int(data['moq']),
                json.dumps(data['details']),
                formated_prices,
                json.dumps(data['userinputs']),
                json.dumps(data['batch_req_details']),
                '{}', #json.dumps(data['purchasestatus']) --JM: Not supported yet
                int(data['id'])
            ]

            DB.db_set(sql_request, sql_params, connection_details=self.connection_details)

        #Categories:
        r = DB.db_get(
            '''
                SELECT
                    partcatlink_id,
                    part_id,
                    category_id
                FROM PartCatLink
                WHERE part_id = %s
            ''', [int(data['id'])], connection_details=self.connection_details)

        toremove = []
        indb = []

        dbcategories = r.to_dict(orient='record')

        for dbcat in dbcategories:

            if dbcat['category_id'] not in data['categories']:
                sql_request = 'DELETE FROM PartCatLink WHERE partcatlink_id = %s'
                sql_params = [
                    dbcat['partcatlink_id']
                ]
                DB.db_set(sql_request, sql_params, connection_details=self.connection_details)

            else:
                indb.append(dbcat['category_id'])

        for cat in data['categories']:
            if cat not in indb:
                sql_request = 'INSERT INTO PartCatLink(part_id, category_id) VALUES(%s,%s)'
                sql_params = [
                    data['id'],
                    cat
                ]
                DB.db_set(sql_request, sql_params, connection_details=self.connection_details)

        #Admins:
        r = DB.db_get(
            '''
                SELECT
                    partadminlink_id,
                    part_id,
                    email_addr
                FROM PartAdminLink, Users
                WHERE part_id = %s and Users.user_id = PartAdminLink.user_id
            ''', [int(data['id'])], connection_details=self.connection_details)

        toremove = []
        indb = []

        alladminemails = []
        for a in data['part_useradmin']:
            alladminemails.append(a['email'])

        dbadmins = r.to_dict(orient='record')

        for dbadmin in dbadmins:

            if dbadmin['email_addr'] not in alladminemails:
                sql_request = 'DELETE FROM PartAdminLink WHERE partadminlink_id = %s'
                sql_params = [
                    dbadmin['partadminlink_id']
                ]
                DB.db_set(sql_request, sql_params, connection_details=self.connection_details)

            else:
                indb.append(dbadmin['email_addr'])

        for a in data['part_useradmin']:
            if a['email'] not in indb:

                #Get user ID (or create if not exists)
                r = DB.db_get(
                '''
                    SELECT
                        user_id
                    FROM Users
                    WHERE email_addr = %s
                ''', [a['email']], connection_details=self.connection_details)

                if len(r) == 0:
                    r = DB.db_get(
                    '''
                        INSERT INTO Users(email_addr, details) VALUES (%s, %s) RETURNING user_id
                    ''', [a['email'],json.dumps({"username": a['username']})], connection_details=self.connection_details)

                usr_id = int(r['user_id'][0])

                sql_request = 'INSERT INTO PartAdminLink(part_id, user_id) VALUES(%s,%s)'
                sql_params = [
                    data['id'],
                    usr_id
                ]
                DB.db_set(sql_request, sql_params, connection_details=self.connection_details)

        return json.dumps({'status': 0})
