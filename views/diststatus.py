# We need to monkey_patch everything
from flask import request
from flask import session
from flask import Response
from flask import make_response
from flask import redirect
from flask_cors import cross_origin

import os
import pathlib
import json
import base64
import ast
import os

import lib.database as DB

class DistStatusView():
    def __init__(self, app):
        self.application = app
        app.add_url_rule('/restapi/distribution/status', 'get_status_list', self.get_status_list, methods=['GET'])

    @cross_origin(supports_credentials=True)
    def get_status_list(self):
        list = self._get_status_list('{}/../distributions_status'.format(pathlib.Path(__file__).parent.absolute()), 'distributions_status')

        if pathlib.Path('{}/../user/scripts/status'.format(pathlib.Path(__file__).parent.absolute())).exists():
            list.extend(self._get_status_list('{}/../user/scripts/status'.format(pathlib.Path(__file__).parent.absolute()), 'status'))

        return json.dumps(
                {
                    'status': 0,
                    'list': list
                }
            )

    def _get_status_list(self, root, location):
        #Get all files of distribution_status folder
        filenames = next(os.walk(root), (None, None, []))[2]  # [] if no file

        status_list = []

        for filename in filenames:
            p = ast.parse(open('{}/{}'.format(root, filename)).read())
            classes = [node for node in ast.walk(p) if isinstance(node, ast.ClassDef)]

            for c in classes:

                classBases = ['{}.{}'.format(n.value.id, n.attr) for n in c.bases]
                if 'diststatus.Status' not in classBases:
                    continue #Skip status registration

                classname = c.name
                classdesc = ast.get_docstring(c)
                if classdesc is None or classdesc == 'None':
                    classdesc = ''

                calldesc = None
                presetdesc = None
                displaydesc = None
                checkisimplemented = False

                cfunctions = [n for n in c.body if isinstance(n, ast.FunctionDef)]

                for f in cfunctions:

                    if f.name == 'call':
                        calldesc = ast.get_docstring(f)
                        if calldesc is None or calldesc == 'None':
                            calldesc = ''

                    if f.name == 'preset':
                        presetdesc = ast.get_docstring(f)
                        if presetdesc is None or presetdesc == 'None':
                            presetdesc = ''

                    if f.name == 'display':
                        displaydesc = ast.get_docstring(f)
                        if displaydesc is None or displaydesc == 'None':
                            displaydesc = ''

                    if f.name == 'check':
                        checkisimplemented = True

                cVar = [n for n in c.body if isinstance(n, ast.Assign)]

                _name = classname
                _author = 'unknown'
                _version = 'unknown'

                for v in cVar:
                    if v.targets[0].id == '__name__':
                        _name = v.value.value
                    if v.targets[0].id == '__author__':
                        _author = v.value.value
                    if v.targets[0].id == '__version__':
                        _version = v.value.value

                if calldesc is None or presetdesc is None or displaydesc is None:
                    print('[Warning] {} in {} has no call and/or preset and/or display function: this status class cannot be used'.format(classname, filename))
                    continue #Skip status registration

                status_list.append(
                        {
                            'id': '{}.{}.{}'.format(location, filename[:-3], classname),
                            'file': filename,
                            'location': location,
                            'name': _name,
                            'author': _author,
                            'version': _version,
                            'classname': classname,
                            'description': classdesc,
                            'call': calldesc,
                            'preset': presetdesc,
                            'display': displaydesc,
                            'check': checkisimplemented
                        }
                    )

        return status_list