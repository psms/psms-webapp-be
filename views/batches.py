# We need to monkey_patch everything
from flask import request
from flask import session
from flask import Response
from flask import make_response
from flask_cors import cross_origin

from psycopg2 import sql

import math
import json
import datetime
import re
import os

import lib.database as DB

class BatchesView():
    def __init__(self, app):
        self.application = app
        app.add_url_rule('/restapi/get_batches', 'get_batches', self.get_batches, methods=['GET'])
        app.add_url_rule('/restapi/get_batches', 'get_batches_with_filter', self.get_batches_with_filter, methods=['POST'])
        app.add_url_rule('/restapi/set_batch', 'set_batch', self.set_batch, methods=['POST'])
        #app.add_url_rule('/restapi/get_part/<id>', 'get_part', self.get_part, methods=['GET'])

        self.connection_details = {
            'DB_HOST': os.environ['DB_HOST'],
            'DB_USER': os.environ['DB_USER'],
            'DB_PASS': os.environ['DB_PASS'],
            'DB_PORT': os.environ['DB_PORT'],
            'DB_DBNAME': os.environ['DB_DBNAME']
        }

    @cross_origin(supports_credentials=True)
    def get_batches(self):
        df = DB.db_get(
            '''
                SELECT
                    Batches.batch_id               as batch_id,
                    Batches.part_id                as part_id,
                    Batches.name                   as name,
                    Batches.planned_date           as planned_date,
                    Batches.actual_date            as actual_date,
                    Batches.quantity               as quantity,
                    Batches.expected_yield         as expected_yield,
                    Batches.details                as details,
                    Parts.name                     as part_name,
                    Parts.description              as part_description,
                    Parts.available                as part_available,
                    Parts.details                  as part_details,
                    Parts.unit_prices              as part_unit_prices,
                    Parts.user_inputs              as part_user_inputs,
                    Parts.batch_req_details        as part_batch_req_details,
                    Parts.purchase_status          as part_purchase_status
                FROM Batches, Parts
                WHERE parts.part_id = batches.part_id
            ''', [], connection_details=self.connection_details)

        res = df.to_dict(orient='record')
        f = []
        for r in res:
            r['details'] = json.loads(r['details'])
            r['part_details'] = json.loads(r['part_details'])
            r['part_unit_prices'] = json.loads(r['part_unit_prices'])
            r['part_user_inputs'] = json.loads(r['part_user_inputs'])
            r['part_batch_req_details'] = json.loads(r['part_batch_req_details'])
            r['part_purchase_status'] = json.loads(r['part_purchase_status'])

            try:
                isempty = math.isnan(r['actual_date'])
                r['actual_date'] = ''
            except:
                isempty = False

            f.append(r)

        return json.dumps({
            'application': 'CERN-DMS-BackEnd',
            'action': 'get_batches',
            'version': '0.0.1',
            'status': 0,
            'batches': f
        })

    @cross_origin(supports_credentials=True)
    def get_batches_with_filter(self):
        data = request.json

        wherereq = ''
        limit = ''
        order_by = ''
        whereparams = []

        #TODO: Protect against SQL Inj.
        for d in data['filters']:
            v = d['val']
            k = d['col']
            c = d['comp']
            cast = ''

            #Supposing that the input pattern is <table>.<column>
            regex = re.compile("[a-zA-Z0-9-_]+\.[a-zA-Z0-9-_]+")

            if not regex.fullmatch(k):
                return json.dumps({'status': -2, 'msg': 'internal error: wrong field name'})

            if c not in ['=','<=','<','>','>=','LIKE']:
                return json.dumps({'status': -2, 'msg': 'internal error: wrong comparator value'})

            if 'cast' in d:
                if d['cast'].lower() not in ['text']:
                    return json.dumps({'status': -2, 'msg': 'internal error: wrong cast value'})

                cast = ' ::{}'.format(d['cast'])

            wherereq += ' AND ({}{}) {} %s'.format(k, cast, c)
            whereparams.append(v)

        if 'limit' in data:
            limit = ' LIMIT {}'.format(int(data['limit']))

        if 'order_by' in data:

            #Supposing that the input pattern is <table>.<column>
            regex = re.compile("[a-zA-Z0-9-_]+\.[a-zA-Z0-9-_]+")

            if not regex.fullmatch(data['order_by']['col']):
                return json.dumps({'status': -2, 'msg': 'internal error: wrong field name'})

            if data['order_by']['order'].lower() not in ['asc','desc']:
                return json.dumps({'status': -2, 'msg': 'internal error: wrong order command'})

            order_by = ' ORDER BY {} {}'.format(sql.Identifier(data['order_by']['col']), sql.Identifier(data['order_by']['order']))

        df = DB.db_get(
            '''
                SELECT
                    Batches.batch_id               as batch_id,
                    Batches.part_id                as part_id,
                    Batches.name                   as name,
                    Batches.planned_date           as planned_date,
                    Batches.actual_date            as actual_date,
                    Batches.quantity               as quantity,
                    Batches.expected_yield         as expected_yield,
                    Batches.details                as details,
                    Parts.name                     as part_name,
                    Parts.description              as part_description,
                    Parts.available                as part_available,
                    Parts.details                  as part_details,
                    Parts.unit_prices              as part_unit_prices,
                    Parts.user_inputs              as part_user_inputs,
                    Parts.batch_req_details        as part_batch_req_details,
                    Parts.purchase_status          as part_purchase_status

                FROM Batches, Parts
                WHERE parts.part_id = batches.part_id {} {} {}
            '''.format(wherereq, order_by, limit), whereparams, connection_details=self.connection_details)


        res = df.to_dict(orient='record')
        f = []
        for r in res:
            r['details'] = json.loads(r['details'])
            r['part_details'] = json.loads(r['part_details'])
            r['part_unit_prices'] = json.loads(r['part_unit_prices'])
            r['part_user_inputs'] = json.loads(r['part_user_inputs'])
            r['part_batch_req_details'] = json.loads(r['part_batch_req_details'])
            r['part_purchase_status'] = json.loads(r['part_purchase_status'])

            try:
                isempty = math.isnan(r['actual_date'])
                r['actual_date'] = ''
            except:
                isempty = False

            f.append(r)

        return json.dumps({
            'application': 'CERN-DMS-BackEnd',
            'action': 'get_batches',
            'version': '0.0.1',
            'status': 0,
            'batches': f
        })

    @cross_origin(supports_credentials=True)
    def set_batch(self):

        '''
            Batches table:
            ==============

            CREATE TABLE Batches(
                batch_id            SERIAL          PRIMARY KEY ,
                part_id             BIGINT                      ,
                name                VARCHAR(256)                ,
                planned_date        TIMESTAMP                   ,
                actual_date         TIMESTAMP                   ,
                quantity            BIGINT                      ,
                expected_yield      FLOAT                       ,
                details             JSONB
            );
        '''

        data_arr = request.json
        sql_requests = []
        line = 0
        errors = []

        for data in data_arr:

            if data['name'] == '':
                errors.append('[row {}] Batch name has to be set'.format(line))

            if data['planned_date'] == '':
                errors.append('[row {}] Planned date has to be set'.format(line))

            try:
                datetime.datetime.strptime(data['planned_date'], '%Y-%m-%d')
            except ValueError:
                errors.append("[row {}] Incorrect data format for planned date, should be YYYY-MM-DD (curent value: '{}')".format(line, data['planned_date']))

            if data['actual_date'] == '':
                data['actual_date'] = None
            else:
                try:
                    datetime.datetime.strptime(data['actual_date'], '%Y-%m-%d')
                except ValueError:
                    errors.append("[row {}] Incorrect data format for actual date, should be YYYY-MM-DD (curent value: '{}')".format(line, data['actual_date']))

            if data['quantity'] == '':
                errors.append('[row {}] Quantity has to be set'.format(line))

            try:
                qty = int(data['quantity'])
            except:
                errors.append('[row {}] Quantity has to be a number'.format(line))

            if data['expected_yield'] == '':
                errors.append('[row {}] Yield has to be set'.format(line))

            try:
                qty = float(data['expected_yield'])
            except:
                errors.append('[row {}] Yield has to be a number'.format(line))

            line += 1

            '''TODO: Check details!!'''

            if int(data['batch_id']) == -1:

                sql_request = 'INSERT INTO Batches(part_id, name, planned_date, actual_date, quantity, expected_yield, details) VALUES(%s,%s,%s,%s,%s,%s,%s)'
                sql_params = [
                    data['part_id'],
                    data['name'],
                    data['planned_date'],
                    data['actual_date'],
                    data['quantity'],
                    data['expected_yield'],
                    json.dumps(data['details'])
                ]

            else:
                sql_request = 'UPDATE Batches SET part_id = %s, name = %s, planned_date = %s, actual_date = %s, quantity = %s, expected_yield = %s, details = %s WHERE batch_id = %s'
                sql_params = [
                    data['part_id'],
                    data['name'],
                    data['planned_date'],
                    data['actual_date'],
                    data['quantity'],
                    data['expected_yield'],
                    json.dumps(data['details']),
                    int(data['batch_id'])
                ]

            sql_requests.append({'r': sql_request, 'p': sql_params})

        if len(errors) > 0:
            return json.dumps({'status': -1, 'errors': errors})

        for sql_r in sql_requests:
            DB.db_set(sql_r['r'], sql_r['p'], connection_details=self.connection_details)

        return json.dumps({'status': 0})
