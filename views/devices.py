# We need to monkey_patch everything
from flask import request
from flask import session
from flask import Response
from flask import make_response
from flask_cors import cross_origin

from psycopg2 import sql

import math
import json
import datetime
import re
import os

import lib.database as DB

class DevicesView():
    def __init__(self, app):
        self.application = app
        app.add_url_rule('/restapi/get_devices', 'get_devices', self.get_devices, methods=['GET'])
        app.add_url_rule('/restapi/get_devices', 'get_devices_with_filter', self.get_devices_with_filter, methods=['POST'])
        app.add_url_rule('/restapi/set_device', 'set_device', self.set_device, methods=['POST'])
        #app.add_url_rule('/restapi/get_part/<id>', 'get_part', self.get_part, methods=['GET'])

        self.connection_details = {
            'DB_HOST': os.environ['DB_HOST'],
            'DB_USER': os.environ['DB_USER'],
            'DB_PASS': os.environ['DB_PASS'],
            'DB_PORT': os.environ['DB_PORT'],
            'DB_DBNAME': os.environ['DB_DBNAME']
        }

    @cross_origin(supports_credentials=True)
    def get_devices(self):
        df = DB.db_get(
            '''
                SELECT
                    Batches.batch_id               as batch_id,
                    Batches.part_id                as batch_part_id,
                    Batches.name                   as batch_name,
                    Batches.planned_date           as batch_planned_date,
                    Batches.actual_date            as batch_actual_date,
                    Batches.quantity               as batch_quantity,
                    Batches.expected_yield         as batch_expected_yield,
                    Batches.details                as batch_details,
                    Devices.device_id              as device_id,
                    Devices.current_status         as current_status,
                    Devices.afterprod_status       as afterprod_status,
                    Devices.details                as details
                FROM Batches, Devices
                WHERE Devices.batch_id = batches.batch_id
            ''', [], connection_details=self.connection_details)

        res = df.to_dict(orient='record')
        f = []
        for r in res:
            r['details'] = json.loads(r['details'])
            r['details'] = json.loads(r['details'])

            try:
                isempty = math.isnan(r['batch_actual_date'])
                r['batch_actual_date'] = ''
            except:
                isempty = False

            try:
                isempty = math.isnan(r['current_status'])
                r['current_status'] = ''
            except:
                isempty = False

            try:
                isempty = math.isnan(r['afterprod_status'])
                r['afterprod_status'] = ''
            except:
                isempty = False


            f.append(r)

        return json.dumps({
            'application': 'CERN-DMS-BackEnd',
            'action': 'get_devices',
            'version': '0.0.1',
            'status': 0,
            'devices': f
        })

    @cross_origin(supports_credentials=True)
    def get_devices_with_filter(self):
        data = request.json

        wherereq = ''
        limit = ''
        order_by = ''
        whereparams = []

        #TODO: Protect against SQL Inj.
        for d in data['filters']:
            v = d['val']
            k = d['col']
            c = d['comp']
            cast = ''

            #Supposing that the input pattern is <table>.<column>
            regex = re.compile("[a-zA-Z0-9-_]+\.[a-zA-Z0-9-_]+")

            if not regex.fullmatch(k):
                return json.dumps({'status': -2, 'msg': 'internal error: wrong field name'})

            if c not in ['=','<=','<','>','>=','LIKE']:
                return json.dumps({'status': -2, 'msg': 'internal error: wrong comparator value'})

            if 'cast' in d:
                if d['cast'].lower() not in ['text']:
                    return json.dumps({'status': -2, 'msg': 'internal error: wrong cast value'})

                cast = ' ::{}'.format(d['cast'])

            wherereq += ' AND ({}{}) {} %s'.format(k, cast, c)
            whereparams.append(v)

        if 'limit' in data:
            limit = ' LIMIT {}'.format(int(data['limit']))

        if 'order_by' in data:

            #Supposing that the input pattern is <table>.<column>
            regex = re.compile("[a-zA-Z0-9-_]+\.[a-zA-Z0-9-_]+")

            if not regex.fullmatch(data['order_by']['col']):
                return json.dumps({'status': -2, 'msg': 'internal error: wrong field name'})

            if data['order_by']['order'].lower() not in ['asc','desc']:
                return json.dumps({'status': -2, 'msg': 'internal error: wrong order command'})

            order_by = ' ORDER BY {} {}'.format(sql.Identifier(data['order_by']['col']), sql.Identifier(data['order_by']['order']))

        df = DB.db_get(
            '''
                SELECT
                    Batches.batch_id               as batch_id,
                    Batches.part_id                as batch_part_id,
                    Batches.name                   as batch_name,
                    Batches.planned_date           as batch_planned_date,
                    Batches.actual_date            as batch_actual_date,
                    Batches.quantity               as batch_quantity,
                    Batches.expected_yield         as batch_expected_yield,
                    Batches.details                as batch_details,
                    Devices.device_id              as device_id,
                    Devices.current_status         as current_status,
                    Devices.afterprod_status       as afterprod_status,
                    Devices.details                as details
                FROM Batches, Devices
                WHERE Devices.batch_id = batches.batch_id {} {} {}
            '''.format(wherereq, order_by, limit), whereparams, connection_details=self.connection_details)

        res = df.to_dict(orient='record')

        f = []
        for r in res:
            r['details'] = json.loads(r['details'])

            try:
                isempty = math.isnan(r['batch_actual_date'])
                r['batch_actual_date'] = ''
            except:
                isempty = False

            try:
                isempty = math.isnan(r['current_status'])
                if int(r['current_status']) == 0:
                    r['current_status'] = 0
                else:
                    r['current_status'] = int(r['current_status'])

            except:
                r['current_status'] = '<unknown>'

            try:
                isempty = math.isnan(r['afterprod_status'])
                if int(r['afterprod_status']) == 0:
                    r['afterprod_status'] = 0
                else:
                    r['afterprod_status'] = int(r['afterprod_status'])

            except:
                r['afterprod_status'] = '<unknown>'

            f.append(r)

        return json.dumps({
            'application': 'CERN-DMS-BackEnd',
            'action': 'get_devices_with_filter',
            'version': '0.0.1',
            'status': 0,
            'devices': f
        })

    @cross_origin(supports_credentials=True)
    def set_device(self):

        '''
            Batches table:
            ==============

            CREATE TABLE Devices(
                device_id           SERIAL          PRIMARY KEY ,
                batch_id            BIGINT                      ,
                current_status      INT                         ,
                afterprod_status    INT                         ,
                details             JSONB
            );
        '''

        data_arr = request.json
        sql_requests = []
        line = 0
        errors = []

        for data in data_arr:

            if data['current_status'] == '':
                errors.append('[row {}] Current status not set'.format(line))

            if data['afterprod_status'] == '':
                errors.append('[row {}] Production status not set'.format(line))

            try:
                tmp = int(data['current_status'])
            except ValueError:
                errors.append("[row {}] Incorrect data format for current status, shall be integer (curent value: '{}')".format(line, data['current_status']))

            try:
                tmp = int(data['afterprod_status'])
            except ValueError:
                errors.append("[row {}] Incorrect data format for production status, shall be integer (curent value: '{}')".format(line, data['afterprod_status']))

            line += 1

            '''TODO: Check details!!'''

            if int(data['device_id']) == -1:

                sql_request = 'INSERT INTO Devices(batch_id, current_status, afterprod_status, details) VALUES(%s,%s,%s,%s)'
                sql_params = [
                    int(data['batch_id']),
                    data['current_status'],
                    data['afterprod_status'],
                    json.dumps(data['details'])
                ]

            else:
                sql_request = 'UPDATE Devices SET batch_id = %s, current_status = %s, afterprod_status = %s, details = %s WHERE device_id = %s'
                sql_params = [
                    int(data['batch_id']),
                    int(data['current_status']),
                    int(data['afterprod_status']),
                    json.dumps(data['details']),
                    int(data['device_id'])
                ]

            sql_requests.append({'r': sql_request, 'p': sql_params})

        if len(errors) > 0:
            return json.dumps({'status': -1, 'errors': errors})

        for sql_r in sql_requests:
            DB.db_set(sql_r['r'], sql_r['p'], connection_details=self.connection_details)

        return json.dumps({'status': 0})
