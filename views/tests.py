# We need to monkey_patch everything
from flask import request
from flask import session
from flask import Response
from flask import make_response
from flask_cors import cross_origin

from psycopg2 import sql

import math
import json
import datetime
import re
import os
import uuid
import io
import pandas
import base64
import matplotlib.pyplot as plt
import matplotlib.dates as mdates

import xlsxwriter


import lib.database as DB
import lib.graphlib as graphlib

class TestView():
    def __init__(self, app):
        self.application = app
        app.add_url_rule('/restapi/get_tests', 'get_tests', self.get_tests, methods=['GET'])
        app.add_url_rule('/restapi/get_tests', 'get_tests_with_filter', self.get_tests_with_filter, methods=['POST'])
        app.add_url_rule('/restapi/get_test_data', 'get_data_with_filter_raw', self.get_data_with_filter_raw, methods=['POST'])
        app.add_url_rule('/restapi/get_test_graph', 'get_data_with_filter_graph', self.get_data_with_filter_graph, methods=['POST'])
        app.add_url_rule('/restapi/set_test', 'set_test', self.set_test, methods=['POST'])
        app.add_url_rule('/restapi/new_test_key', 'set_test_key', self.set_test_key, methods=['GET'])
        app.add_url_rule('/restapi/get_test_keys', 'get_test_keys', self.get_test_keys, methods=['GET'])
        app.add_url_rule('/restapi/get_test_excel/<test_id>', 'get_test_excel', self.get_test_excel, methods=['GET'])

        self.connection_details = {
            'DB_HOST': os.environ['DB_HOST'],
            'DB_USER': os.environ['DB_USER'],
            'DB_PASS': os.environ['DB_PASS'],
            'DB_PORT': os.environ['DB_PORT'],
            'DB_DBNAME': os.environ['DB_DBNAME']
        }

    @cross_origin(supports_credentials=True)
    def set_test_key(self):
        keystr = uuid.uuid4().hex

        df = DB.db_set(
            '''
                INSERT INTO TesterConnectionKeys (key_str, gen_date, enabled)
                VALUES (%s, NOW(), %s)
            ''', [keystr, True], connection_details=self.connection_details)

        return json.dumps({
            'application': 'CERN-DMS-BackEnd',
            'action': 'get_test_keys',
            'version': '0.0.1',
            'status': 0,
            'key_str': keystr
        })

    @cross_origin(supports_credentials=True)
    def get_test_keys(self):
        df = DB.db_get(
            '''
                SELECT
                    TesterConnectionKeys.key_str       as key_str,
                    TesterConnectionKeys.gen_date      as gen_date,
                    TesterConnectionKeys.enabled       as enabled

                FROM TesterConnectionKeys
                WHERE enabled = %s
            ''', [True], connection_details=self.connection_details)

        res = df.to_dict(orient='record')

        return json.dumps({
            'application': 'CERN-DMS-BackEnd',
            'action': 'get_test_keys',
            'version': '0.0.1',
            'status': 0,
            'keys': res
        })


    @cross_origin(supports_credentials=True)
    def get_tests(self):
        df = DB.db_get(
            '''
                SELECT
                    TestInstances.test_id       as test_id,
                    TestInstances.date          as test_date,
                    TestInstances.details       as test_details,
                    TestInstances.summary       as test_summary,
                    TestInstances.pass          as test_pass,

                    Devices.device_id           as device_id,
                    Devices.name                as device_name,
                    Devices.current_status      as device_currstatus,
                    Devices.afterprod_status    as device_afterprodstatus,
                    Devices.details             as device_details

                FROM TestInstances, Devices
                WHERE Devices.device_id = TestInstances.device_id
            ''', [], connection_details=self.connection_details)

        df['device_name'] = df['device_name'].fillna('Unknown')

        res = df.to_dict(orient='record')
        f = []
        for r in res:
            r['test_details'] = json.loads(r['test_details'])
            r['device_details'] = json.loads(r['device_details'])

            f.append(r)

        return json.dumps({
            'application': 'CERN-DMS-BackEnd',
            'action': 'get_tests',
            'version': '0.0.1',
            'status': 0,
            'tests': f
        })

    @cross_origin(supports_credentials=True)
    def get_data_with_filter_graph(self):
        data = request.json

        r = self.get_data_with_filter(data)
        if r['status'] < 0:
            return json.dumps(r)

        xy_x = '{}_{}'.format(data['xy-xcol'].replace('.','_'),  data['xy-xkey'].replace('.','_'))
        xy_y = '{}_{}'.format(data['xy-ycol'].replace('.','_'),  data['xy-ykey'].replace('.','_'))

        barchart_x = '{}_{}'.format(data['barchart-xcol'].replace('.','_'),  data['barchart-xkey'].replace('.','_'))
        barchart_y = '{}_{}'.format(data['barchart-ycol'].replace('.','_'),  data['barchart-ykey'].replace('.','_'))

        hist_axis = '{}_{}'.format(data['hist-col'].replace('.','_'),  data['hist-key'].replace('.','_'))

        glib = graphlib.GraphLib()

        try:
            if data['type'] == 'xy':
                fig, ax = glib.traceXYGraph(r['data'], xy_x.lower(), xy_y.lower(), type=data['xy-kind'])
            elif data['type'] == 'barchart':
                fig, ax = glib.traceBarGraph(r['data'], barchart_x.lower(), barchart_y.lower(), agg=data['barchart-agg'])
            elif data['type'] == 'histogram':
                fig, ax = glib.traceHistGraph(r['data'], hist_axis.lower(), bins=100, op=None, filled=False)
        except Exception as e:
            return json.dumps({'status': 'error', 'str': str(e)})

        plt.xticks(rotation=45)
        box = ax.get_position()
        ax.set_position([box.x0, box.y0, box.width*0.65, box.height])
        legend_x = 1
        legend_y = 0.5
        plt.legend(loc='center left', bbox_to_anchor=(legend_x, legend_y))

        plt.tight_layout()

        figfile = io.BytesIO()
        plt.savefig(figfile, format='png', bbox_inches="tight")
        figfile.seek(0)
        figdata_png = figfile.getvalue()

        figdata_png = base64.b64encode(figdata_png)
        img_str = 'data:image/png;base64,{}'.format(str(figdata_png, "utf-8"))

        return json.dumps({
            'application': 'CERN-DMS-BackEnd',
            'action': 'get_data_with_filter_graph',
            'version': '0.0.1',
            'status': 0,
            'graph': img_str
        })

    @cross_origin(supports_credentials=True)
    def get_data_with_filter_raw(self):
        data = request.json

        r = self.get_data_with_filter(data)
        if r['status'] < 0:
            return json.dumps(r)

        return json.dumps({
            'application': 'CERN-DMS-BackEnd',
            'action': 'get_data_with_filter_raw',
            'version': '0.0.1',
            'status': 0,
            'tests': r['data'].to_dict(orient='record')
        })

    def get_data_with_filter(self, data):
        data = request.json

        wherereq = []
        limit = ''
        order_by = ''
        whereparams = []
        tables = []
        selectreq = []

        #TODO: Protect against SQL Inj.
        for d in data['filters']:

            v = d['val']
            k = d['col']
            c = d['comp']
            cast = ''

            t = k.split('.')[0]

            #Supposing that the input pattern is <table>.<column>
            regex = re.compile("[a-zA-Z0-9-_]+\.[a-zA-Z0-9-_]+")

            if not regex.fullmatch(k):
                return {'status': -2, 'msg': 'internal error: wrong field name'}

            if d['key'] != '':
                k = '({} -> \'{}\')'.format(d['col'], d['key'])

            if c not in ['=','<=','<','>','>=','LIKE']:
                return {'status': -2, 'msg': 'internal error: wrong comparator value'}

            if 'cast' in d and d['cast'] != 'none' and d['cast'] != 'null':
                if d['cast'].lower() not in ['text','int','float','date','boolean']:
                    return {'status': -2, 'msg': 'internal error: wrong cast value \'{}\''.format(d['cast'])}

                cast = ' ::{}'.format(d['cast'])

            wherereq.append('({}{}) {} %s'.format(k, cast, c))
            whereparams.append(v)

            if t not in tables:
                tables.append(t)

        #TODO: Protect against SQL Inj.
        for d in data['data']:

            k = d['col']
            t = k.split('.')[0]

            #Supposing that the input pattern is <table>.<column>
            regex = re.compile("[a-zA-Z0-9-_]+\.[a-zA-Z0-9-_]+")

            if not regex.fullmatch(k):
                return {'status': -2, 'msg': 'internal error: wrong field name'}

            if d['key'] != '':
                k = '{} -> \'{}\''.format(d['col'], d['key'])

            selectreq.append(' {} as {}_{}'.format(k, d['col'].replace('.','_'),  d['key'].replace('.','_')))

            if t not in tables:
                tables.append(t)


        if 'limit' in data:
            limit = ' LIMIT {}'.format(int(data['limit']))

        if 'order_by' in data:

            #Supposing that the input pattern is <table>.<column>
            regex = re.compile("[a-zA-Z0-9-_]+\.[a-zA-Z0-9-_]+")

            if not regex.fullmatch(data['order_by']['col']):
                return {'status': -2, 'msg': 'internal error: wrong field name'}

            if data['order_by']['order'].lower() not in ['asc','desc']:
                return {'status': -2, 'msg': 'internal error: wrong order command'}

            order_by = ' ORDER BY {} {}'.format(sql.Identifier(data['order_by']['col']), sql.Identifier(data['order_by']['order']))

        #Build link between tables
        needtable = {
            'TestInstances': {
                'TestInstances': [],
                'TestTypes': ['SubTestInstances'],
                'SubTestInstances': [],
                'Devices': [],
                'MeasurementInstances': []
            },
            'TestTypes':{
                'TestInstances': ['SubTestInstances'],
                'TestTypes': [],
                'SubTestInstances': [],
                'Devices': ['SubTestInstances','TestInstances'],
                'MeasurementInstances': []
            },
            'SubTestInstances': {
                'TestInstances': [],
                'TestTypes': [],
                'SubTestInstances': [],
                'Devices': ['TestInstances'],
                'MeasurementInstances': []
            },
            'Devices': {
                'TestInstances': [],
                'TestTypes': ['SubTestInstances','TestInstances'],
                'SubTestInstances': ['TestInstances'],
                'Devices': [],
                'MeasurementInstances': []
            },
            'MeasurementInstances': {
                'TestInstances': [],
                'TestTypes': [],
                'SubTestInstances': [],
                'Devices': [],
                'MeasurementInstances': []
            }
        }

        for t0 in tables:
            for t1 in tables:
                if len(needtable[t0][t1]) > 0:
                    for k in needtable[t0][t1]:
                        if k not in tables:
                            tables.append(k)

        wherelink = {
            'TestInstances': {
                'TestInstances': None,
                'TestTypes': None,
                'SubTestInstances': 'SubTestInstances.test_id = TestInstances.test_id',
                'Devices': 'TestInstances.device_id = Devices.device_id',
                'MeasurementInstances': 'MeasurementInstances.test_id = TestInstances.test_id'
            },
            'TestTypes':{
                'TestInstances': None,
                'TestTypes': None,
                'SubTestInstances': 'SubTestInstances.testtype_id = TestTypes.testtype_id',
                'Devices': None,
                'MeasurementInstances': 'MeasurementInstances.testtype_id = TestTypes.testtype_id'
            },
            'SubTestInstances': {
                'TestInstances': 'SubTestInstances.test_id = TestInstances.test_id',
                'TestTypes': 'SubTestInstances.testtype_id = TestTypes.testtype_id',
                'SubTestInstances': None,
                'Devices': None,
                'MeasurementInstances': 'MeasurementInstances.subtest_id = SubTestInstances.subtest_id'
            },
            'Devices': {
                'TestInstances': 'TestInstances.device_id = Devices.device_id',
                'TestTypes': None,
                'SubTestInstances': None,
                'Devices': None,
                'MeasurementInstances': 'MeasurementInstances.device_id = Devices.device_id'
            },
            'MeasurementInstances': {
                'TestInstances': 'MeasurementInstances.test_id = TestInstances.test_id',
                'TestTypes': 'MeasurementInstances.testtype_id = TestTypes.testtype_id',
                'SubTestInstances': 'MeasurementInstances.subtest_id = SubTestInstances.subtest_id',
                'Devices': 'MeasurementInstances.device_id = Devices.device_id',
                'MeasurementInstances': None
            }
        }

        for t0 in tables:
            for t1 in tables:
                if wherelink[t0][t1] is not None:
                    wherereq.append(wherelink[t0][t1])

        if len(wherereq) > 0:
            whereinst = 'WHERE {}'.format(' and '.join(wherereq))
        else:
            whereinst = ''

        df = DB.db_get(
            '''
                SELECT
                    {}
                FROM {}
                {}
            '''.format(','.join(selectreq), ', '.join(tables), whereinst, order_by, limit), whereparams, connection_details=self.connection_details)

        df.fillna('', inplace=True)

        return {
            'status': 0,
            'data': df
        }
        '''
        json.dumps({
            'application': 'CERN-DMS-BackEnd',
            'action': 'get_tests_with_filter',
            'version': '0.0.1',
            'status': 0,
            'tests': df.to_dict(orient='record')
        })
        '''

    @cross_origin(supports_credentials=True)
    def get_tests_with_filter(self):
        data = request.json

        wherereq = ''
        limit = ''
        order_by = ''
        whereparams = []

        #TODO: Protect against SQL Inj.
        for d in data['filters']:
            v = d['val']
            k = d['col']
            c = d['comp']
            cast = ''

            #Supposing that the input pattern is <table>.<column>
            regex = re.compile("[a-zA-Z0-9-_]+\.[a-zA-Z0-9-_]+")

            if not regex.fullmatch(k):
                return json.dumps({'status': -2, 'msg': 'internal error: wrong field name'})

            if c not in ['=','<=','<','>','>=','LIKE']:
                return json.dumps({'status': -2, 'msg': 'internal error: wrong comparator value'})

            if 'cast' in d:
                if d['cast'].lower() not in ['text']:
                    return json.dumps({'status': -2, 'msg': 'internal error: wrong cast value'})

                cast = ' ::{}'.format(d['cast'])

            wherereq += ' AND ({}{}) {} %s'.format(k, cast, c)
            whereparams.append(v)

        if 'limit' in data:
            limit = ' LIMIT {}'.format(int(data['limit']))

        if 'order_by' in data:

            #Supposing that the input pattern is <table>.<column>
            regex = re.compile("[a-zA-Z0-9-_]+\.[a-zA-Z0-9-_]+")

            if not regex.fullmatch(data['order_by']['col']):
                return json.dumps({'status': -2, 'msg': 'internal error: wrong field name'})

            if data['order_by']['order'].lower() not in ['asc','desc']:
                return json.dumps({'status': -2, 'msg': 'internal error: wrong order command'})

            order_by = ' ORDER BY {} {}'.format(sql.Identifier(data['order_by']['col']), sql.Identifier(data['order_by']['order']))

        df = DB.db_get(
            '''
                SELECT
                    TestInstances.test_id       as test_id,
                    TestInstances.date          as test_date,
                    TestInstances.details       as test_details,
                    TestInstances.summary       as test_summary,
                    TestInstances.pass          as test_pass,

                    Devices.device_id           as device_id,
                    Devices.name                as device_name,
                    Devices.current_status      as device_currstatus,
                    Devices.afterprod_status    as device_afterprodstatus,
                    Devices.details             as device_details

                FROM TestInstances, Devices
                WHERE Devices.device_id = TestInstances.device_id {} {} {}
            '''.format(wherereq, order_by, limit), whereparams, connection_details=self.connection_details)

        res = df.to_dict(orient='record')

        f = []
        for r in res:
            r['test_details'] = json.loads(r['test_details'])
            r['test_summary'] = json.loads(r['test_summary'])
            r['device_details'] = json.loads(r['device_details'])

            f.append(r)

        return json.dumps({
            'application': 'CERN-DMS-BackEnd',
            'action': 'get_tests_with_filter',
            'version': '0.0.1',
            'status': 0,
            'tests': f
        })

    @cross_origin(supports_credentials=True)
    def set_test(self):

        data_errors = []

        try:
            data = request.json
        except:
            return json.dumps({'status': -1, 'errors': ['JSON file is missing']})

        if 'key' not in data:
            return json.dumps({'status': -1, 'errors': ['Authentication key is missing (key parameter)'] })

        df = DB.db_get(
            '''
                SELECT
                    TesterConnectionKeys.tester_id       as tester_id
                FROM TesterConnectionKeys
                WHERE enabled = %s AND key_str = %s
            ''', [True, data['key']], connection_details=self.connection_details)

        if len(df) < 1:
            return json.dumps({'status': -1, 'errors': ['Authentication key not found ({})'.format(data['key'])]})

        req_keys = ['pass', 'details', 'device', 'subtests']
        for key in req_keys:
            if key not in data:
                data_errors.append('{} key is missing'.format(key))

        #Check subtests:
        req_keys = ['test_type_name', 'name', 'details', 'summary', 'pass']
        for subtest in data['subtests']:
            for key in req_keys:
                if key not in subtest:
                    data_errors.append('{} key is missing in subtest : {}'.format(key, subtest))

        if len(data_errors) > 0:
            return json.dumps({'status': -2, 'errors': data_errors})

        #Batch specified: Search or create batch if not exist
        batch_id = None
        wherereq = ''
        whereparams = []

        if 'batch' in data['device']:
            if 'name' in data['device']['batch']:
                wherereq += 'AND name LIKE %s '
                whereparams.append(data['device']['batch']['name'])

            if 'details' in data['device']['batch']:
                for key in data['device']['batch']['details']:
                    wherereq += 'AND details->>\'{}\' = %s '.format(key)
                whereparams.append(str(data['device']['batch']['details'][key]))

            df = DB.db_get(
            '''
                SELECT
                    batch_id
                FROM Batches
                WHERE batch_id > 0 {}
            '''.format(wherereq), whereparams, connection_details=self.connection_details)

            if len(df) == 1:
                batch_id = int(df['batch_id'][0])

            else:
                return json.dumps({'status': -3, 'errors': ['Batch not found']})

        #Search or Add device
        wherereq = ''
        whereparams = []

        if 'name' not in data['device']:
            return json.dumps({'status': -4, 'errors': ['Device name is not set']})

        wherereq += 'AND name LIKE %s '
        whereparams.append(data['device']['name'])

        device_details = {}

        if 'details' in data['device']:
            for key in data['device']['details']:
                wherereq += 'AND details->>\'{}\' = %s '.format(key)
                whereparams.append(str(data['device']['details'][key]))

        df = DB.db_get(
        '''
            SELECT
                device_id
            FROM Devices
            WHERE device_id > 0 {}
        '''.format(wherereq), whereparams, connection_details=self.connection_details)

        if len(df) == 1:
            device_id = int(df['device_id'][0])

        else:
            df = DB.db_get(
            '''
                INSERT INTO Devices (batch_id, name, current_status, afterprod_status, details)
                VALUES (%s, %s, %s, %s, %s) RETURNING device_id
            ''', [batch_id, data['device']['name'], data['pass'], data['pass'], json.dumps(data['device']['details'])], connection_details=self.connection_details)

            if len(df) != 1:
                return json.dumps({'status': -4, 'errors': ['Error when inserting device']})

            device_id = int(df['device_id'][0])

        DB.db_set('UPDATE Devices SET current_status = %s WHERE device_id = %s', [data['pass'], device_id])

        #Add test instance
        test_disp = {}
        if 'display' in data:
            test_disp = data['display']

        test_summary = {}
        if 'summary' in data:
            test_summary = data['summary']

        df = DB.db_get(
        '''
            INSERT INTO TestInstances (device_id, date, details, summary, display, pass)
            VALUES (%s, NOW(), %s, %s, %s, %s) RETURNING test_id
        ''', [device_id, json.dumps(data['details']), json.dumps(test_summary), json.dumps(test_disp), data['pass']], connection_details=self.connection_details)

        if len(df) != 1:
            return json.dumps({'status': -5, 'errors': 'Error when inserting test instance'})

        test_id = int(df['test_id'][0])

        #Add all subtests and test types if not exist
        test_types = {}

        for subtest in data['subtests']:

            #test type
            if subtest['test_type_name'] not in test_types:
                df = DB.db_get(
                '''
                    SELECT
                        testtype_id
                    FROM TestTypes
                    WHERE name = %s
                ''', [subtest['test_type_name']], connection_details=self.connection_details)

                if len(df) == 1:
                    test_type_id = int(df['testtype_id'][0])

                else:
                    test_type_details = {}
                    if 'test_type_details' in subtest:
                        test_type_details = subtest['test_type_details']

                    df = DB.db_get(
                    '''
                        INSERT INTO TestTypes (name, details)
                        VALUES (%s, %s) RETURNING testtype_id
                    ''', [subtest['test_type_name'], json.dumps(test_type_details)], connection_details=self.connection_details)

                    if len(df) != 1:
                        return json.dumps({'status': -6, 'errors': ['Error when inserting test type']})

                    test_type_id = int(df['testtype_id'][0])
            else:
                test_type_id = test_types[subtest['test_type_name']]

            #Push subtest
            df = DB.db_get(
                '''
                    INSERT INTO SubTestInstances (test_id, testtype_id, name, details, summary, pass)
                    VALUES (%s, %s, %s, %s, %s, %s) RETURNING subtest_id
                ''', [test_id, test_type_id, subtest['name'], json.dumps(subtest['details']), json.dumps(subtest['summary']), subtest['pass']], connection_details=self.connection_details)

            if len(df) != 1:
                return json.dumps({'status': -7, 'errors': ['Error when inserting subtest']})

            subtest_id = int(df['subtest_id'][0])

            value = ()
            values = []
            #Push measurements
            if 'measurements' in subtest and subtest['measurements']:

                for meas in subtest['measurements']:
                    measpass = -1
                    if 'pass' in meas:
                        measpass = meas['pass']

                    measdata = {}
                    if 'data' in meas:
                        measdata = meas['data']

                    measname = ''
                    if 'name' in meas:
                        measname = meas['name']

                    value = (test_id, test_type_id, subtest_id, device_id,
                             measname, json.dumps(measdata), measpass)
                    values.append(value)

                    query = "INSERT INTO MeasurementInstances (test_id, testtype_id, subtest_id, device_id, name, datapoint, pass) VALUES"

                df = DB.db_set_multi(
                    sql_request=query, params=values, connection_details=self.connection_details)

        return json.dumps({
            'application': 'CERN-DMS-BackEnd',
            'action': 'set_test',
            'version': '0.0.1',
            'status': 0,
            'test_id': test_id
        })

    @cross_origin(supports_credentials=True)
    def get_test_excel(self, test_id):

        df = DB.db_get(
            '''
                SELECT
                    TestInstances.test_id       as test_id,
                    TestInstances.date          as test_date,
                    TestInstances.details       as test_details,
                    TestInstances.summary       as test_summary,
                    TestInstances.pass          as test_pass,

                    Devices.device_id           as device_id,
                    Devices.name                as device_name,
                    Devices.current_status      as device_currstatus,
                    Devices.afterprod_status    as device_afterprodstatus,
                    Devices.details             as device_details,

                    TestTypes.TestType_id       as testtype_id,
                    TestTypes.name              as testtype_name,
                    TestTypes.Details           as testtype_Details,

                    SubTestInstances.subtest_id as subtestinstances_subtest_id,
                    SubTestInstances.name       as subtestinstances_name,
                    SubTestInstances.details    as subtestinstances_details,
                    SubTestInstances.pass       as subtestinstances_pass,
                    SubTestInstances.summary    as subtestinstances_summary

                FROM TestInstances, Devices, TestTypes, SubTestInstances
                WHERE Devices.device_id = TestInstances.device_id
                AND SubTestInstances.test_id = TestInstances.test_id
                AND TestInstances.test_id = %s
                AND TestTypes.TestType_id = SubTestInstances.TestType_id

            ''', [test_id], connection_details=self.connection_details)

        print('LEN DF : {}'.format(len(df)))

        if len(df) <= 0:
            return 'No data found ..'

        print(df)

        #Init excel file
        output = io.BytesIO()
        writer = pandas.ExcelWriter(output, engine='xlsxwriter')
        workbook  = writer.book

        bold = workbook.add_format({'bold': True})
        endbold = workbook.add_format({'bold': False})

        worksheet = workbook.add_worksheet('Test details')

        title_format = workbook.add_format({
                'bold': 1,
                'border': 1,
                'align': 'center',
                'valign': 'vcenter',
                'bg_color': '#595959',
                'color': 'white'
            })
        subtitle_format = workbook.add_format({
                'bold': 1,
                'border': 0,
                'align': 'center',
                'valign': 'vcenter',
                'color': '#4F81BD'
            })

        bold_cell = workbook.add_format({
                'bold': True
            })

        ''' Write test detail page '''
        '''========================'''

        # Add "Generic details" title
        worksheet.merge_range('A1:E1', 'General details', title_format)

        '''1st column: Test information'''
        '''----------------------------'''

        # Get details
        test_date = df['test_date'].values[0]
        test_pass = df['test_pass'].values[0]
        test_description = json.loads(df['test_details'].values[0])

        worksheet.merge_range('A2:B2', 'Test information', subtitle_format) # Add "Test information" title

        # Write test information and compute width
        row = 3
        firstColWidth = 0
        secondColWidth = 0

        # Write date
        column_verbose = 'Date'

        worksheet.write('A{}'.format(row), '{}: '.format(column_verbose), bold_cell)
        if(len(column_verbose) > firstColWidth):
            firstColWidth = len(column_verbose)

        worksheet.write('B{}'.format(row), '{}'.format(test_date))
        if(len('{}'.format(test_date)) > secondColWidth):
            secondColWidth = len('{}'.format(test_date))

        row = row + 1

        # Write pass result
        column_verbose = 'Passed'

        worksheet.write('A{}'.format(row), '{}: '.format(column_verbose), bold_cell)
        if(len(column_verbose) > firstColWidth):
            firstColWidth = len(column_verbose)

        worksheet.write('B{}'.format(row), '{}'.format(test_pass))
        if(len('{}'.format(test_pass)) > secondColWidth):
            secondColWidth = len('{}'.format(test_pass))

        row = row + 1

        for key in test_description:
            # Write column 1 (column name)
            column_verbose = key.capitalize()

            worksheet.write('A{}'.format(row), '{}: '.format(column_verbose), bold_cell)
            if(len(column_verbose) > firstColWidth):
                firstColWidth = len(column_verbose)

            # Write column 2 (Value)
            worksheet.write('B{}'.format(row), '{}'.format(test_description[key]))
            if(len('{}'.format(test_description[key])) > secondColWidth):
                secondColWidth = len('{}'.format(test_description[key]))

            row = row + 1

        # Set column width
        worksheet.set_column(0, 0, firstColWidth+1)
        worksheet.set_column(1, 1, secondColWidth+1)

        testDescRowCnt = row

        '''2nd column: Device information'''
        '''------------------------------'''

        # Get details
        eq_name = df['device_name'].values[0]
        eq_description = json.loads(df['device_details'].values[0])
        if type(eq_description) is not dict:
            eq_description = {'description': '{}'.format(df['device_details'].values[0])}



        # Add title
        worksheet.merge_range('D2:E2', 'Device information', subtitle_format)

        # Write device information and compute width
        row = 3
        firstColWidth = 0
        secondColWidth = 0

        # Write date
        column_verbose = 'Name'

        worksheet.write('D{}'.format(row), '{}: '.format(column_verbose), bold_cell)
        if(len(column_verbose) > firstColWidth):
            firstColWidth = len(column_verbose)

        worksheet.write('E{}'.format(row), '{}'.format(eq_name))
        if(len('{}'.format(eq_name)) > secondColWidth):
            secondColWidth = len('{}'.format(eq_name))

        row = row + 1

        for key in eq_description:
            # Write column 1 (column name)
            column_verbose = key.capitalize()

            worksheet.write('D{}'.format(row), '{}: '.format(column_verbose), bold_cell)
            if(len(column_verbose) > firstColWidth):
                firstColWidth = len(column_verbose)

            # Write column 2 (Value)
            worksheet.write('E{}'.format(row), '{}'.format(eq_description[key]))
            if(len('{}'.format(eq_description[key])) > secondColWidth):
                secondColWidth = len('{}'.format(eq_description[key]))

            row = row + 1

        # Set column width
        worksheet.set_column(3, 3, firstColWidth+1)
        worksheet.set_column(4, 4, secondColWidth+1)

        '''3rd column: Tests information'''
        '''-----------------------------'''
        worksheet.merge_range('G1:H1', 'Test(s) performed', title_format)

        dfGrp = df.groupby('testtype_name')

        # Write device information and compute width
        row = 2
        firstColWidth = 0
        secondColWidth = 0

        for name, group in dfGrp:
            # Get details
            testtypes_description = json.loads(group['testtype_details'].values[0])
            if type(testtypes_description) is not dict:
                testtypes_description = {'description': '{}'.format(df['testtypes_details'].values[0])}

            # Add TestType title
            worksheet.merge_range(row-1, 6, row-1, 8, name, subtitle_format)
            row += 1

            column_verbose = 'Status'

            worksheet.write('G{}'.format(row), '{}: '.format(column_verbose), bold_cell)
            if(len(column_verbose) > firstColWidth):
                firstColWidth = len(column_verbose)

            # Test Type status
            if group['subtestinstances_pass'].min() == 0:
                value = 'Passed'
            else:
                value = 'Failed ({})'.format(group['subtestinstances_pass'].min())

            worksheet.write('H{}'.format(row), '{}'.format(value))
            if(len('{}'.format(value)) > secondColWidth):
                secondColWidth = len('{}'.format(value))

            row += 1

            for key in testtypes_description:
                # Write column 1 (column name)
                column_verbose = key.capitalize()

                worksheet.write('G{}'.format(row), '{}: '.format(column_verbose), bold_cell)
                if(len(column_verbose) > firstColWidth):
                    firstColWidth = len(column_verbose)

                # Write column 2 (Value)
                worksheet.write('H{}'.format(row), '{}'.format(testtypes_description[key]))
                if(len('{}'.format(testtypes_description[key])) > secondColWidth):
                    secondColWidth = len('{}'.format(testtypes_description[key]))

                row = row + 1

        # Set column width
        reqlen = len('Test(s) performed')
        if (firstColWidth+secondColWidth) < reqlen:
            secondColWidth = reqlen - firstColWidth

        worksheet.set_column(6, 6, firstColWidth+1)
        worksheet.set_column(7, 7, secondColWidth+1)

        ''' Write measurement pages '''
        '''========================='''
        #Test measurements:
        for name, group in dfGrp:

            ''' List all of the keys '''
            list_keys = ['Run', 'Name']
            for i in group.index:
                desc = json.loads(group.at[i,'subtestinstances_details'])
                for key in desc.keys():
                    if key not in list_keys:
                        list_keys.append(key)
            for i in group.index:
                desc = json.loads(group.at[i,'subtestinstances_summary'])
                for key in desc.keys():
                    if key not in list_keys:
                        list_keys.append(key)
            list_keys.append('Passed')

            print('Keys for {}: {}'.format(name, list_keys))

            # Create subtest dedicated page
            worksheet = workbook.add_worksheet(name)

            # Group by subtest instances
            subtest = group.groupby('subtestinstances_subtest_id')
            subtest_cnt = 0

            colLen = []
            colCnt = len(list_keys)

            for i in range(colCnt):
                colLen.append(0)

            row = 0
            col = 0

            for key in list_keys:
                column_verbose = '{}: '.format(key.capitalize())
                worksheet.write(row, col, column_verbose, bold_cell)
                colLen[col] = len(column_verbose)
                col += 1

            row += 1

            for subtestid, resdf in subtest:

                subTest_description = json.loads(resdf['subtestinstances_details'].values[0])
                subTest_description.update(json.loads(resdf['subtestinstances_summary'].values[0]))
                subTest_pass = resdf['subtestinstances_pass'].values[0]
                subTest_name = resdf['subtestinstances_name'].values[0]

                col = 0
                for key in list_keys:
                    if key == 'Run':
                        worksheet.write_number(row, col, subtest_cnt)

                    elif key == 'Name':
                        worksheet.write(row, col, subTest_name)

                    elif key == 'Passed':
                        worksheet.write(row, col, "Passed" if subTest_pass == 0 else "Failed")

                    elif key in subTest_description:
                        try:
                            worksheet.write_number(row, col, subTest_description[key])
                        except:
                            worksheet.write(row, col, '{}'.format(subTest_description[key]))

                    col += 1

                subtest_cnt += 1
                row += 1



            for i in range(0, len(colLen)):
                worksheet.set_column(i, i, colLen[i]+1)

        workbook.close()
        writer.save()

        print("Writer done")

        response = make_response(output.getvalue())
        response.headers['Content-Disposition'] = 'attachment; filename=testing_report.xlsx'
        response.mimetype='application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'
        return response


