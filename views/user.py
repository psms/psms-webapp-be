# We need to monkey_patch everything
from flask import request
from flask import session
from flask import Response
from flask import make_response
from flask import redirect
from flask_cors import cross_origin
from authlib.integrations.flask_client import OAuth

import json
import base64
import os
import lib.database as DB
import lib.signon as signon

class UserView():
    def __init__(self, app):
        self.application = app
        app.add_url_rule('/restapi/user', 'get_user', self.get_user, methods=['GET'])
        app.add_url_rule('/restapi/user/get_connection_info', 'get_connection_info', self.get_connection_info, methods=['GET'])
        app.add_url_rule('/restapi/user/connected', 'oauth_connect', self.oauth_connect, methods=['GET'])
        app.add_url_rule('/restapi/user/disconnected', 'disconnected', self.disconnected, methods=['GET'])

        self.signon = signon.SignON(app)

    @cross_origin(supports_credentials=True)
    def get_user(self):
        try:
            userdetails = self.signon.get_user_details()
        except:
            return json.dumps(
                {
                    'status': -1,
                    'msg': 'Not connected'
                }
            )

        return json.dumps(
            {
                'status': 0,
                'user': userdetails
            }
        )

    @cross_origin(supports_credentials=True)
    def disconnected(self):
        if 'token' in session:
            session.pop('token')
        return redirect(request.args.get('r'))

    @cross_origin(supports_credentials=True)
    def get_connection_info(self):
        try:
            redirect = request.args.get('r')
            if not redirect:
                raise Exception('No redirection found')

        except:
            return {
                    'status': -2,
                    'msg': 'No redirect URL found'
                }

        try:
            userdetails = self.signon.get_user_details()
            userdetails.pop('user_id')
            connected = True
        except Exception as e:
            userdetails = {}
            connected = False

        return  json.dumps({
            'status': 0,
            'connect_url': self.signon.get_connection_url(redirect),
            'disconnect_url': self.signon.get_disconnect_url(redirect),
            'connected': connected,
            'user': userdetails
        })

    @cross_origin(supports_credentials=True)
    def oauth_connect(self):
        self.signon.oauth_connect()
        return redirect(request.args.get('r'))