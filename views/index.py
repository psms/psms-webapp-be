# We need to monkey_patch everything
from flask import request
from flask import session
from flask import Response
from flask import make_response

import json
import os

class IndexView():
    def __init__(self, app):
        self.application = app
        app.add_url_rule('/restapi', 'index', self.index, methods=['GET'])

    def index(self):
        return json.dumps({
            'application': 'CERN-DMS-BackEnd',
            'version': '0.0.1'
        })