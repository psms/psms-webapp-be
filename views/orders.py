# We need to monkey_patch everything
from flask import request
from flask import session
from flask import Response
from flask import make_response
from flask_cors import cross_origin

import json
import importlib
import traceback
import sys
import pathlib
import os
import math

import lib.database as DB
import lib.signon as signon
import user.notifications as notifications

if pathlib.Path('{}/../user/scripts/status'.format(pathlib.Path(__file__).parent.absolute())).exists():
    sys.path.append('{}/../user/scripts'.format(pathlib.Path(__file__).parent.absolute()))

class OrdersView():
    def __init__(self, app):
        self.application = app
        app.add_url_rule('/restapi/order/config', 'get_order_config', self.get_order_config, methods=['GET'])
        app.add_url_rule('/restapi/order/config', 'set_order_config', self.set_order_config, methods=['POST'])
        app.add_url_rule('/restapi/order', 'set_order', self.set_order, methods=['POST'])
        app.add_url_rule('/restapi/order/presets', 'preset_next_status', self.preset_next_status, methods=['POST'])
        app.add_url_rule('/restapi/order/next', 'call_next_status', self.call_next_status, methods=['POST'])
        app.add_url_rule('/restapi/order/display', 'get_status_display', self.get_status_display, methods=['POST'])
        app.add_url_rule('/restapi/orders', 'get_order_parts', self.get_order_parts, methods=['GET'])
        app.add_url_rule('/restapi/myorders', 'get_user_order', self.get_user_order, methods=['GET'])
        app.add_url_rule('/restapi/order/cancel', 'cancel_order', self.cancel_order, methods=['POST'])
        app.add_url_rule('/restapi/order/admincancel', 'admincancel_order', self.admincancel_order, methods=['POST'])
        app.add_url_rule('/restapi/order/soldqty', 'sold_quantity', self.sold_quantity, methods=['POST'])

        self.signon = signon.SignON(app)
        self.connection_details = {
            'DB_HOST': os.environ['DB_HOST'],
            'DB_USER': os.environ['DB_USER'],
            'DB_PASS': os.environ['DB_PASS'],
            'DB_PORT': os.environ['DB_PORT'],
            'DB_DBNAME': os.environ['DB_DBNAME']
        }

    def get_order(self, order_id=None, user_id=None):
        #init
        df = DB.db_get(
            '''
                SELECT
                    OrderConfig.order_userinputs         as order_userinputs,
                    OrderConfig.shipment_userinputs      as shipment_userinputs,
                    OrderConfig.purchase_status          as purchase_status
                FROM OrderConfig
                ORDER BY orderconfig_id DESC LIMIT 1
            ''', [], connection_details=self.connection_details)

        config = df.to_dict(orient='record')
        status_list = {}

        purchase_status = json.loads(config[0]['purchase_status'])
        for s in purchase_status:
            status_list[s['status_id']] = s

        status_list[-1] = {
            'id': '',
            'call': '',
            'file': '',
            'name': 'Wait for acknowledgement',
            'check': False,
            'preset': '',
            'display': '',
            'status_id': -1,
            'description': 'Initialization - wait for administrator acknowledgement'
        }

        status_list[0] = {
            'id': '',
            'call': '',
            'file': '',
            'name': 'Done',
            'check': False,
            'preset': '',
            'display': '',
            'status_id': 0,
            'description': 'Done'
        }

        status_list[-2] = {
            'id': '',
            'call': '',
            'file': '',
            'name': 'Canceled',
            'check': False,
            'preset': '',
            'display': '',
            'status_id': -2,
            'description': 'Canceled'
        }

        sqlreq = '''
                SELECT
                    PurchaseRequest.purchaserequest_id    as purchaserequest_id              ,
                    PurchaseRequest.user_id               as user_id                         ,
                    PurchaseRequest.status                as status                          ,
                    PurchaseRequest.next_status           as next_status                     ,
                    PurchaseRequest.order_date            as order_date                      ,
                    PurchaseRequest.discount              as discount                        ,
                    PurchaseRequest.details               as details                         ,
                    PurchaseRequest.shipment              as shipment                        ,
                    PurchaseRequest.tobeshipped           as tobeshipped                     ,

                    PartPreqLink.partpreqlink_id          as PartPreqLink_partpreqlink_id    ,
                    PartPreqLink.part_id                  as PartPreqLink_part_id            ,
                    PartPreqLink.purchaserequest_id       as PartPreqLink_purchaserequest_id ,
                    PartPreqLink.act_quantity             as PartPreqLink_actquantity        ,
                    PartPreqLink.ori_quantity             as PartPreqLink_oriquantity        ,
                    PartPreqLink.user_inputs              as PartPreqLink_user_inputs        ,

                    PurchaseMgt.purchasemgt_id            as PurchaseMgt_purchasemgt_id      ,
                    PurchaseMgt.partpreqlink_id           as PurchaseMgt_purchaserequest_id  ,
                    PurchaseMgt.batch_id                  as PurchaseMgt_batch_id            ,
                    PurchaseMgt.status                    as PurchaseMgt_status              ,
                    PurchaseMgt.next_status               as PurchaseMgt_next_status         ,
                    PurchaseMgt.quantity                  as PurchaseMgt_quantity            ,
                    PurchaseMgt.discount                  as PurchaseMgt_discount            ,
                    PurchaseMgt.details                   as PurchaseMgt_details             ,

                    Users.email_addr                      as user_email_addr                 ,
                    Users.details                         as user_details                    ,

                    Parts.name                            as part_name                       ,
                    Parts.description                     as part_description                ,
                    Parts.available                       as part_available                  ,
                    Parts.details                         as part_details                    ,
                    Parts.unit_prices                     as part_unit_prices                ,
                    Parts.user_inputs                     as part_user_inputs                ,
                    Parts.batch_req_details               as part_batch_req_details          ,
                    Parts.purchase_status                 as part_purchase_status	         ,

                    PartAdminLink.user_id				  as admin_user_id                   ,
                    AdminUsers.email_addr			      as admin_email_addr                ,
                    AdminUsers.details   			      as admin_user_details

                FROM Users, PurchaseRequest
                LEFT JOIN PartPreqLink ON PartPreqLink.purchaserequest_id = PurchaseRequest.purchaserequest_id
                LEFT JOIN Parts ON Parts.part_id = PartPreqLink.part_id
                LEFT JOIN PurchaseMgt ON PurchaseMgt.partpreqlink_id = PartPreqLink.partpreqlink_id
                LEFT JOIN PartAdminLink ON PartAdminLink.part_id = Parts.part_id
                LEFT JOIN Users as AdminUsers ON AdminUsers.user_id = PartAdminLink.user_id
                WHERE Users.user_id = PurchaseRequest.user_id
            '''

        sqlparams = []

        if order_id != None:
            sqlreq += ' AND PurchaseRequest.purchaserequest_id = %s'
            sqlparams.append(order_id)

        if user_id != None:
            sqlreq += ' AND PurchaseRequest.user_id = %s'
            sqlparams.append(user_id)

        #Get orders
        df = DB.db_get(sqlreq, sqlparams, connection_details=self.connection_details)

        ordersdf = df.groupby('purchaserequest_id')

        orders = []

        for id, orderdf in ordersdf:
            orderdetails = orderdf.to_dict('records')

            partsgrps = orderdf.groupby('partpreqlink_partpreqlink_id')
            parts = []

            for partsgrps_id, partsgrps_df in partsgrps:

                partrequests = partsgrps_df.to_dict('records')

                partmgts = []
                totalassignedtobatch = 0

                adminids = []
                admins = []
                
                for partrequest in partrequests:
                    try:
                        tmp = math.isnan(partrequest['PurchaseMgt_purchasemgt_id'])
                        isempty = False
                    except:
                        isempty = True

                    if isempty == False:
                        partmgts.append({
                            'id': partrequest['purchasemgt_purchasemgt_id'],
                            'batch_id': partrequest['purchasemgt_batch_id'],
                            'details': json.loads(partrequest['purchasemgt_details']),
                            'discount': partrequest['purchasemgt_discount'],
                            'status': int(partrequest['purchasemgt_status']),
                            'next_status': int(partrequest['purchasemgt_next_status']),
                            'quantity': int(partrequest['purchasemgt_quantity']),
                        })

                        totalassignedtobatch += int(partrequest['purchasemgt_quantity'])

                    try:
                        isempty = math.isnan(partrequest['admin_user_id'])
                    except:
                        isempty = False

                    if not isempty and partrequest['admin_user_id'] not in adminids:
                        adminids.append(partrequest['admin_user_id'])

                        try:
                            username = json.loads(partrequest['admin_user_details'])['username']
                        except:
                            username = 'unknown'


                        print(partrequest)

                        admins.append({
                            'email': partrequest['admin_email_addr'],
                            'username': username,
                            'details': json.loads(partrequest['admin_user_details'])
                        })

                parts.append({
                    'admins' : admins,
                    'partpreq_id': partrequests[0]['partpreqlink_partpreqlink_id'],
                    'id': partrequests[0]['partpreqlink_part_id'],
                    'act_quantity': int(partrequests[0]['partpreqlink_actquantity']),
                    'ori_quantity': int(partrequests[0]['partpreqlink_oriquantity']),
                    'qty_assigned': totalassignedtobatch,
                    'userinputs': json.loads(partrequests[0]['partpreqlink_user_inputs']),
                    'part_name': partrequests[0]['part_name'],
                    'part_description': partrequests[0]['part_description'],
                    'part_available': True if partrequests[0]['part_available'] == 't' else False,
                    'part_details': json.loads(partrequests[0]['part_details']),
                    'part_unit_prices': json.loads(partrequests[0]['part_unit_prices']),
                    'part_user_inputs': json.loads(partrequests[0]['part_user_inputs']),
                    'part_batch_req_details': json.loads(partrequests[0]['part_batch_req_details']),
                    'part_purchase_status': json.loads(partrequests[0]['part_purchase_status'])
                })

            #next_status = status_list[0]
            if orderdetails[0]['next_status'] in status_list:
                next_status = status_list[orderdetails[0]['next_status']]
            elif orderdetails[0]['status'] <= 0:
                next_status  = {}
            else:
                raise Exception('Next status {} does not exist'.format(orderdetails[0]['next_status']))

            orders.append({
                'id': orderdetails[0]['purchaserequest_id'],
                'order_date': orderdetails[0]['order_date'],
                'shipment': json.loads(orderdetails[0]['shipment']),
                'tobeshipped': True if orderdetails[0]['tobeshipped'] == 't' else False,
                'curr_status': status_list[orderdetails[0]['status']],
                'next_status': next_status,
                'details': json.loads(orderdetails[0]['details']),
                'discount': orderdetails[0]['discount'],
                'ordered_by_email': orderdetails[0]['user_email_addr'],
                'ordered_by_id': orderdetails[0]['user_id'],
                'ordered_by_details': json.loads(orderdetails[0]['user_details']),
                'parts': parts
            })

        return {'order': orders, 'status_list': status_list}

    @cross_origin(supports_credentials=True)
    def sold_quantity(self):

        try:
            part_id = int(request.json['part_id'])
        except:
            return json.dumps({'status': -1, 'errors': ['Order id missed-formatted']})

        df = DB.db_get(
            '''
                SELECT
                    SUM(PartPreqLink.act_quantity) as sold_quantity

                FROM PurchaseRequest, PartPreqLink
                WHERE PurchaseRequest.status != -1 AND PurchaseRequest.purchaserequest_id = PartPreqLink.purchaserequest_id AND PartPreqLink.part_id = %s
            ''', [part_id], connection_details=self.connection_details)

        if len(df) != 1:
            return json.dumps({'status': -1, 'errors': ['internal error']})

        res = df.to_dict(orient='record')

        return json.dumps({
            'application': 'CERN-DMS-BackEnd',
            'action': 'sold_quantity',
            'version': '0.0.1',
            'status': 0,
            'quantity': res[0]['sold_quantity']
        })


    @cross_origin(supports_credentials=True)
    def cancel_order(self):

        try:
            order_id = int(request.json['order_id'])
        except:
            return json.dumps({'status': -1, 'errors': ['Order id missed-formatted']})

        #Get user ID
        try:
            user = self.signon.get_user_details()
            #if 'email' not in data or data['email'] != user['email']:
            #    return json.dumps({'status': -1, 'errors': ['Connection issue: not connected']})
        except Exception:
            return json.dumps({'status': -2, 'errors': ['Connection issue: not connected']})

        order = self.get_order(order_id= order_id, user_id= user['user_id'])

        if len(order['order']) != 1:
            return json.dumps({'status': -4, 'errors': ['Order not found']})

        if order['order'][0]['curr_status']['status_id'] == -1:
            DB.db_set(
                '''
                    UPDATE PurchaseRequest
                        SET next_status = %s, status = %s
                    WHERE purchaserequest_id = %s
                ''', [None, -2, order_id], connection_details=self.connection_details)

            return json.dumps({'status': 0})

        return json.dumps({'status': -3, 'errors': ['Status is invalid for cancellation']})

    @cross_origin(supports_credentials=True)
    def admincancel_order(self):
        try:
            order_id = int(request.json['order_id'])
        except:
            return json.dumps({'status': -1, 'errors': ['Order id missed-formatted']})

        DB.db_set(
            '''
                UPDATE PurchaseRequest
                    SET next_status = %s, status = %s
                WHERE purchaserequest_id = %s
            ''', [None, -2, order_id], connection_details=self.connection_details)

        return json.dumps({'status': 0})

    @cross_origin(supports_credentials=True)
    def get_order_config(self):
        df = DB.db_get(
            '''
                SELECT
                    OrderConfig.order_userinputs         as order_userinputs,
                    OrderConfig.shipment_userinputs      as shipment_userinputs,
                    OrderConfig.purchase_status          as purchase_status
                FROM OrderConfig
                ORDER BY orderconfig_id DESC LIMIT 1
            ''', [], connection_details=self.connection_details)

        res = df.to_dict(orient='record')


        if len(res) != 1:
            return json.dumps({'status': -1, 'msg': 'internal error: no configuration'})

        purchase_status = json.loads(res[0]['purchase_status'])

        status_list = {}
        for s in purchase_status:
            status_list[s['status_id']] = s

        status_list[-1] = {
            'id': '',
            'call': '',
            'file': '',
            'name': 'Wait for acknowledgement',
            'check': False,
            'preset': '',
            'display': '',
            'status_id': -1,
            'description': 'Initialization - wait for administrator acknowledgement'
        }

        status_list[0] = {
            'id': '',
            'call': '',
            'file': '',
            'name': 'Done',
            'check': False,
            'preset': '',
            'display': '',
            'status_id': 0,
            'description': 'Done'
        }

        status_list[-2] = {
            'id': '',
            'call': '',
            'file': '',
            'name': 'Canceled',
            'check': False,
            'preset': '',
            'display': '',
            'status_id': -2,
            'description': 'Canceled'
        }

        f = {
            'order_userinputs': json.loads(res[0]['order_userinputs']),
            'shipment_userinputs': json.loads(res[0]['shipment_userinputs']),
            'purchase_status': status_list,
        }

        return json.dumps({
            'application': 'CERN-DMS-BackEnd',
            'action': 'get_order_config',
            'version': '0.0.1',
            'status': 0,
            'config': f
        })

    @cross_origin(supports_credentials=True)
    def set_order_config(self):
        data = request.json

        errors = []

        if 'order_userinputs' not in data:
            errors.append('Order inputs not set')

        if 'shipment_userinputs' not in data:
            errors.append('Shipments inputs not set')

        if 'purchase_status' not in data:
            errors.append('Order status list not set')

        if len(errors) > 0:
            return json.dumps({'status': -1, 'errors': errors})

        sql_request = 'INSERT INTO OrderConfig(order_userinputs, shipment_userinputs, purchase_status) VALUES(%s,%s,%s)'
        sql_params = [
            json.dumps(data['order_userinputs']),
            json.dumps(data['shipment_userinputs']),
            json.dumps(data['purchase_status'])
        ]

        DB.db_set(sql_request, sql_params, connection_details=self.connection_details)

        return json.dumps({'status': 0})

    @cross_origin(supports_credentials=True)
    def preset_next_status(self):
        data = request.json

        if 'order_id' not in data:
            return json.dumps({'status': -1, 'errors': ['order id missing']})

        try:
            order = self.get_order(data['order_id'])
            order_details = order['order']
            status_list = order['status_list']

            user = self.signon.get_user_details()

            if order_details[0]['next_status']['status_id'] == 0: #Done status
                return {'status': 0, 'order_id': data['order_id'], 'inputs': []}
            elif order_details[0]['next_status']['status_id'] == -1: #-1: acknowledge status
                return {'status': 0, 'order_id': data['order_id'], 'inputs': []}
            elif order_details[0]['next_status']['status_id'] == 0: #-2: cancel status
                return {'status': 0, 'order_id': data['order_id'], 'inputs': []}

            module = importlib.import_module("{}.{}".format(order_details[0]['next_status']['location'], order_details[0]['next_status']['file'][:-3]))
            class_ = getattr(module, order_details[0]['next_status']['classname'])
            instance = class_(user, data['order_id'], order_details[0], status_list)
            inputs = instance.preset()
        except Exception as e:
            return json.dumps({'status': -2, 'errors': [
                    {'msg': str(e), 'desc': traceback.format_exc()}
                ]
            })

        return json.dumps({'status': 0, 'order_id': data['order_id'], 'inputs': inputs})

    @cross_origin(supports_credentials=True)
    def call_next_status(self):
        data = request.json

        if 'order_id' not in data:
            return json.dumps({'status': -1, 'errors': ['order id missing']})

        try:
            order = self.get_order(data['order_id'])
            order_details = order['order']
            status_list = order['status_list']

            user = self.signon.get_user_details()

            if order_details[0]['next_status']['status_id'] == 0: #Done status
                DB.db_set(
                    '''
                        UPDATE PurchaseRequest
                            SET next_status = %s, status = %s
                        WHERE purchaserequest_id = %s
                    ''', [None, 0, data['order_id']], connection_details=self.connection_details)

                return json.dumps({'status': 0})

            elif order_details[0]['next_status']['status_id'] == -1: #-1: acknowledge status
                return json.dumps({'status': 0})
            elif order_details[0]['next_status']['status_id'] == 0: #-2: cancel status
                return json.dumps({'status': 0})

            module = importlib.import_module("{}.{}".format(order_details[0]['next_status']['location'], order_details[0]['next_status']['file'][:-3]))
            class_ = getattr(module, order_details[0]['next_status']['classname'])
            instance = class_(user, data['order_id'], order_details[0], status_list)
            callret = instance._call(order_details[0]['next_status']['status_id'], data['inputs'])
        except Exception as e:
            return json.dumps({'status': -2, 'errors': [
                    {'msg': str(e), 'desc': traceback.format_exc()}
                ]
            })

        return json.dumps(callret)

    @cross_origin(supports_credentials=True)
    def get_status_display(self):
        data = request.json

        if 'order_id' not in data:
            return json.dumps({'status': -1, 'errors': ['order id missing']})

        try:
            order = self.get_order(data['order_id'])
            order_data = order['order']
            status_list = order['status_list']

            if order_data[0]['curr_status']['status_id'] == 0: #Done status
                return json.dumps({'status': 0, 'display': {}})

            elif order_data[0]['curr_status']['status_id'] == -1: #-1: acknowledge status
                return json.dumps({'status': 0, 'display': {}})

            elif order_data[0]['curr_status']['status_id'] == 0: #-2: cancel status
                return json.dumps({'status': 0, 'display': {}})

            user = self.signon.get_user_details()

            module = importlib.import_module("{}.{}".format(order_data[0]['curr_status']['location'], order_data[0]['curr_status']['file'][:-3]))
            class_ = getattr(module, order_data[0]['curr_status']['classname'])
            instance = class_(user, data['order_id'], order_data[0], status_list)
            callret = instance.display()
        except Exception as e:
            return json.dumps({'status': -2, 'errors': [
                    {'msg': str(e), 'desc': traceback.format_exc()}
                ]
            })

        return json.dumps({'status': 0, 'display': callret})

    @cross_origin(supports_credentials=True)
    def get_order_parts(self):
        try:
            return json.dumps({'status': 0, 'orders': self.get_order()['order']})
        except Exception as e:
            return json.dumps({'status': -1, 'msg': str(e)})


    @cross_origin(supports_credentials=True)
    def get_user_order(self):
        #data = request.json

        errors = []

        #Get user ID
        try:
            user = self.signon.get_user_details()
            print(user)
            #if 'email' not in data or data['email'] != user['email']:
            #    return json.dumps({'status': -1, 'errors': ['Connection issue: not connected']})
        except Exception:
            return json.dumps({'status': -1, 'errors': ['Connection issue: not connected']})

        try:
            return json.dumps({'status': 0, 'orders': self.get_order(user_id=user['user_id'])['order']})
        except Exception as e:
            return json.dumps({'status': -2, 'msg': str(e)})



    @cross_origin(supports_credentials=True)
    def set_order(self):
        data = request.json

        '''
            {
                "username": " ",
                "details": {
                    "key": "val"
                },
                "shipping": {
                    "key": "val"
                },
                "basket": {
                    "parts": [
                        {
                            "part_id": "",
                            "quantity": "",
                            "user_inputs": {
                                "key": "val"
                            }
                        },
                        ...
                    ]
                    "packages": [
                        {
                            "package_id": "",
                            "quantity": "",
                            "user_inputs": {
                                "key": "val"
                            }
                        },
                        ...
                    ]
                }
            }
        '''

        errors = []

        #Get user ID
        try:
            user = self.signon.get_user_details()
            if 'email' not in data or data['email'] != user['email']:
                return json.dumps({'status': -1, 'errors': ['Connection issue: not connected']})
        except Exception:
            return json.dumps({'status': -1, 'errors': ['Connection issue: not connected']})

        #Details
        df = DB.db_get(
            '''
                SELECT
                    OrderConfig.order_userinputs         as order_userinputs,
                    OrderConfig.shipment_userinputs      as shipment_userinputs,
                    OrderConfig.purchase_status          as purchase_status
                FROM OrderConfig
                ORDER BY orderconfig_id DESC LIMIT 1
            ''', [], connection_details=self.connection_details)

        res = df.to_dict(orient='record')

        if len(res) != 1:
            return json.dumps({'status': -1, 'errors': ['internal error: no configuration']})


        f = {
            'order_userinputs': json.loads(res[0]['order_userinputs']),
            'shipment_userinputs': json.loads(res[0]['shipment_userinputs']),
            'purchase_status': json.loads(res[0]['purchase_status']),
        }

        for order_input in f['order_userinputs']:
            print(order_input)
            if 'optional' not in order_input['name']: #and order_input['optional'] == False:
                if order_input['name'] not in data['details'] or data['details'][order_input['name']] == '':
                    errors.append('Missing details: {} has to be set'.format(order_input['name']))

        if 'shipping' in data:
            shipment = True
            shipdetails = data['shipping']

            for shipment_input in f['shipment_userinputs']:
                if 'optional' not in shipment_input['name']: #and shipment_input['optional'] == False:
                    if shipment_input['name'] not in data['shipping'] or data['shipping'][shipment_input['name']] == '':
                        errors.append('Missing shipment details: {} has to be set'.format(shipment_input['name']))

        else:
            shipdetails = {}
            shipment = False

        if len(errors) > 0:
            return json.dumps({'status': -2, 'errors': errors})

        '''
            CREATE TABLE PurchaseRequest(
                purchaserequest_id  SERIAL          PRIMARY KEY ,
                user_id             BIGINT                      ,
                status              INT                         ,
                next_status         INT                         ,
                order_date          TIMESTAMP                   ,
                discount            FLOAT                       ,
                details             JSONB                       ,
                shipment            JSONB                       ,
                tobeshipped         BOOLEAN
            );
        '''

        sql_request = 'INSERT INTO PurchaseRequest(user_id, status, next_status, order_date, discount, details, shipment, tobeshipped) VALUES(%s,-1,1,NOW(),0,%s,%s,%s) RETURNING purchaserequest_id'
        sql_params = [
            int(user['user_id']),
            json.dumps(data['details']),
            json.dumps(shipdetails),
            shipment
        ]

        df = DB.db_get(sql_request, sql_params, connection_details=self.connection_details)
        purchaserequest_id = int(df['purchaserequest_id'][0])

        for part in data['basket']['parts']:
            sql_request = 'INSERT INTO PartPreqLink(part_id, purchaserequest_id, ori_quantity, act_quantity, user_inputs) VALUES(%s,%s,%s,%s,%s)'
            sql_params = [
                int(part['part_id']),
                purchaserequest_id,
                int(part['quantity']),
                int(part['quantity']),
                json.dumps(part['user_inputs'])
            ]

            DB.db_set(sql_request, sql_params, connection_details=self.connection_details)

        sql_request = '''SELECT
            PurchaseRequest.purchaserequest_id    as purchaserequest_id              ,
            PurchaseRequest.user_id               as user_id                         ,

            PartPreqLink.partpreqlink_id          as PartPreqLink_partpreqlink_id    ,
            PartPreqLink.purchaserequest_id       as PartPreqLink_purchaserequest_id ,
            PartPreqLink.act_quantity             as PartPreqLink_actquantity        ,
            PartPreqLink.user_inputs              as PartPreqLink_user_inputs        ,

            Users.email_addr                      as user_email_addr                 ,
            Users.details                         as user_details                    ,

            Parts.name                            as part_name                       ,
            
            PartAdminLink.user_id				  as admin_user_id                   ,
            AdminUsers.email_addr			      as admin_email_addr                ,
            AdminUsers.details   			      as admin_user_details

        FROM Users, PurchaseRequest
        LEFT JOIN PartPreqLink ON PartPreqLink.purchaserequest_id = PurchaseRequest.purchaserequest_id
        LEFT JOIN Parts ON Parts.part_id = PartPreqLink.part_id
        LEFT JOIN PurchaseMgt ON PurchaseMgt.partpreqlink_id = PartPreqLink.partpreqlink_id
        LEFT JOIN PartAdminLink ON PartAdminLink.part_id = Parts.part_id
        LEFT JOIN Users as AdminUsers ON AdminUsers.user_id = PartAdminLink.user_id
        WHERE Users.user_id = PurchaseRequest.user_id
        '''
        sql_request += f'AND PurchaseRequest.purchaserequest_id = {df["purchaserequest_id"][0]}'
        df = DB.db_get(sql_request, [], connection_details=self.connection_details)

        ordersdf = df.groupby('purchaserequest_id')

        for id, orderdf in ordersdf:
            partsgrps = orderdf.groupby('partpreqlink_partpreqlink_id')
            parts = []

            for _, partsgrps_df in partsgrps:

                partrequests = partsgrps_df.to_dict('records')
                adminids = []

                admins = []
                for partrequest in partrequests:
                    try:
                        isempty = math.isnan(partrequest['admin_user_id'])
                    except:
                        isempty = False

                    if not isempty and partrequest['admin_user_id'] not in adminids:
                        adminids.append(partrequest['admin_user_id'])

                        try:
                            username = json.loads(partrequest['admin_user_details'])['username']
                        except:
                            username = 'unknown'
                        admins.append({
                            'email': partrequest['admin_email_addr'],
                            'username': username,
                            'details': json.loads(partrequest['admin_user_details'])
                        })

                parts.append({
                    'admins' : admins,
                    'partpreq_id': partrequests[0]['partpreqlink_partpreqlink_id'],
                    'act_quantity': int(partrequests[0]['partpreqlink_actquantity']),
                    'userinputs': json.loads(partrequests[0]['partpreqlink_user_inputs']),
                    'part_name': partrequests[0]['part_name'],
                })

        mail = notifications.EmailNotifier(user)
        mail.send_creation()
        mail = notifications.ManagerEmailNotifier(user)
        mail.send_creation(id = df['purchaserequest_id'][0], parts = parts)

        return json.dumps({'status': 0})
