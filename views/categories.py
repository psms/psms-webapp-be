# We need to monkey_patch everything
from flask import request
from flask import session
from flask import Response
from flask import make_response
from flask_cors import cross_origin

import json
import os
import lib.database as DB

class PARTException(Exception):
    pass

class CategoriesView():
    def __init__(self, app):
        self.application = app
        app.add_url_rule('/restapi/get_categories', 'get_categories', self.get_categories, methods=['GET'])
        app.add_url_rule('/restapi/set_category', 'set_category', self.set_category, methods=['POST'])
        #app.add_url_rule('/restapi/get_part/<id>', 'get_part', self.get_part, methods=['GET'])

        self.connection_details = {
            'DB_HOST': os.environ['DB_HOST'],
            'DB_USER': os.environ['DB_USER'],
            'DB_PASS': os.environ['DB_PASS'],
            'DB_PORT': os.environ['DB_PORT'],
            'DB_DBNAME': os.environ['DB_DBNAME']
        }

    @cross_origin(supports_credentials=True)
    def get_categories(self):
        r = DB.db_get('SELECT * from Categories', [], connection_details=self.connection_details)

        return json.dumps({
            'application': 'CERN-DMS-BackEnd',
            'action': 'get_categories',
            'version': '0.0.1',
            'status': 0,
            'categories': r.to_dict(orient='records')
        })


    @cross_origin(supports_credentials=True)
    def set_category(self):

        '''
        CREATE TABLE Categories(
            category_id         SERIAL          PRIMARY KEY ,
            name                VARCHAR(256)                ,
            description         TEXT                        ,
            manadatory_details  JSONB
        );
        '''
        data = request.json
        errors = []

        if data['name'] == '':
            errors.append('Part name has to be set')

        if data['description'] == '':
            errors.append('Part description has to be set')

        if len(errors) > 0:
            return json.dumps({'status': -1, 'errors': errors})

        if int(data['id']) == -1:

            sql_request = 'INSERT INTO Categories(name, description, mandatory_details) VALUES(%s,%s,%s)'
            sql_params = [
                data['name'],
                data['description'],
                json.dumps(data['mandatory_details'])
            ]

            DB.db_set(sql_request, sql_params, connection_details=self.connection_details)
            return json.dumps({'status': 0})

        else:
            sql_request = 'UPDATE Categories SET name = %s, description = %s, mandatory_details = %s WHERE category_id = %s'
            sql_params = [
                data['name'],
                data['description'],
                json.dumps(data['mandatory_details']),
                int(data['id'])
            ]

            DB.db_set(sql_request, sql_params, connection_details=self.connection_details)
            return json.dumps({'status': 0})
