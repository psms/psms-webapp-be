import enum
from numpy import equal
from requests.auth import HTTPBasicAuth  # or HTTPDigestAuth, or OAuth1, etc.
from requests import Session
from zeep import Client
from zeep.transports import Transport
from lxml import etree
import pprint

import requests
import os

import lib.diststatus as diststatus

class GenerateTID(diststatus.Status):
    '''
        # TID Generator

        The acknowledged status checks whether all of the devices have been associated to batch or not!
        Once they have been successfully associated, the status can be changed to the Acknowledged one.
        If one of the device is not ready, the update will fail.
    '''

    #Status details
    __name__    = '[CORE] TID Generator'
    __author__  = 'Julian Maxime Mendez'
    __version__ = 'v.1.0'

    #Call function
    def call(self, inputs):
        '''
            Acknowledging a request means that all parts have been associated to batches and are
            ready to be shipped.

            Therefore, before aprouving the "Acknowledged" status, the call function checks that
            all of the parts are linked to a production.
        '''
        print(inputs)

        if len(inputs['Title']) <= 40:
            title = inputs['Title']
        else:
            raise Exception('The title is too long - must be under 40 characters.')

        parts = []
        unitprice = 0
        proccode = 0
        for part in self.order_details['parts']:
            unitprice = part['part_unit_prices'][0]['chf']
            if len(part['part_unit_prices']) > 1:
                for price, index in enumerate(part['part_unit_prices']):
                    if (part['act_quantity'] >= price['qty'] and part['act_quantity'] < part['part_unit_prices'][index+1]['qty'] ):
                        unitprice = price['chf']
                    elif( part['act_quantity'] >= part['part_unit_prices'][index+1]['qty']):
                        unitprice = part['part_unit_prices'][index+1]['chf']

            if (inputs['Discount (%)'] != '0'):
                unitprice =  float(unitprice) * float((inputs['Discount (%)']))/100
            else:
                unitprice = float(unitprice)

            proccode = ''
            for d in part['part_details']:
                if d['name'] == 'EDH Proc. code':
                    proccode = d['value']
                    break
            if proccode == '':
                raise Exception('EDH proc. code not found for {}'.format(part['part_name']))

            supplierBudgetCode = ''
            for d in part['part_details']:
                if 'Credit Budget Code' in d['name']:
                    supplierBudgetCode = d['value']
                    break
            if supplierBudgetCode == '':
                if 'Supplier budget code for {}'.format(part['part_name']) in inputs:
                    if not inputs['Supplier budget code for {}'.format(part['part_name'])] == '':
                        supplierBudgetCode = inputs['Supplier budget code for {}'.format(part['part_name'])]
                    else:
                        raise Exception('Supplier budget code not provided for {}'.format(part['part_name']))
                else:
                    raise Exception('Supplier budget code not found for {}'.format(part['part_name']))


            parts.append({
                'qty': int(part['act_quantity']),
                'unitprice' : unitprice,
                'short_desc' : part['part_name'],
                'long_desc': part['part_description'],
                'proccode': proccode,
                'supplierBudgetCode': supplierBudgetCode
            })

        documentNumber = self.createTID(
                title,
                self.user['details']['cern_person_id'],
                self.order_details['ordered_by_details']['cern_person_id'],
                inputs['Client budget code'],
                parts
            )

        self.order_details['discount'] = inputs['Discount (%)']

        self.add_order_details( {"TID" : documentNumber  })


        if "TID status" not in self.order_details["details"]:
            self.add_order_details( {"TID status": self.recoverDocumentStatus(self.order_details["details"]["TID"]) } )
        else:
            self.order_details["details"]["TID status"] = self.recoverDocumentStatus(self.order_details["details"]["TID"])

        if 'Skip Shipping' in self.order_details:
            if self.order_details['Skip Shipping']:
                return self.status_id + 2

        return self.status_id+1

    #Preset function
    def preset(self):
        '''
            An option allows skiping the checking.
        '''
        print(self.order_details['parts'])
        title = 'asic_distrib_{}_#{}'.format(self.order_details["ordered_by_details"]["family_name"].replace(" ","_"),self.order_details['id'])
        discount = self.order_details['discount']
        budget_code = self.order_details['details']['Budget Code']
        # budget_code = 27401

        ret = [
            {
                'type': 'text',
                'name': 'Title',
                'desc': 'TID Title',
                'default': title,
                'options': []
            },
            {
                'type': 'text',
                'name': 'Client budget code',
                'desc': 'CERN Budget code',
                'default': '{}'.format(budget_code),
                'options': []
            }
        ]

        for part in self.order_details['parts']:
            found = False

            for d in part['part_details']:
                if 'Credit Budget Code' in d['name']:
                    ret.append(
                        {
                            'type': 'text',
                            'name': 'Supplier budget code for {}'.format(part['part_name']),
                            'desc': 'Budget code to be credited.',
                            'default': d['value'],
                            'options': []
                        }
                    )
                    found = True
                    break

            if not found:
                ret.append(
                    {
                        'type': 'text',
                        'name': 'Supplier budget code for {}'.format(part['part_name']),
                        'desc': 'Budget code to be credited.',
                        'default': '',
                        'options': []
                    }
                )

        ret.append(
            {
                'type': 'text',
                'name': 'Discount (%)',
                'desc': 'General discount in %',
                'default': discount,
                'options': []
            }
        )

        return ret

    #Display function
    def display(self):
        '''
            The display function shows the purchase status for each part.
            It will allow to know whether all of the parts are ready or not.
        '''
        if "TID status" not in self.order_details["details"]:
            self.add_order_details( {"TID status": self.recoverDocumentStatus(self.order_details["details"]["TID"]) } )
        else:
            self.order_details["details"]["TID status"] = self.recoverDocumentStatus(self.order_details["details"]["TID"])

        return {
            "TID created for this order" : f'<a href=\"http://edh.cern.ch/Document/{self.order_details["details"]["TID"]}\">http://edh.cern.ch/Document/{self.order_details["details"]["TID"]}</a>',
            "TID status": self.order_details["details"]["TID status"]
        }

    #Check function
    def check(self):
        return True

    def recoverDocumentStatus(self, docId):
        session = Session()
        session.auth = HTTPBasicAuth(os.environ['EDHLogin'], os.environ['EDHPasswd'])
        session.verify = False
        transport_with_basic_auth = Transport(session=session)
        client = Client(
            wsdl='http://devedhws.cern.ch/ws/services/EDHDocumentImport?wsdl',
            transport=transport_with_basic_auth
        )
        node = client.create_message(client.service, 'getDocumentStatus', docId = 9191959)

        url="https://devedhws.cern.ch/ws/services/EDHDocumentImport"
        headers = {'content-type': 'text/xml'}
        body = etree.tostring(node, pretty_print=True)
        response = requests.post(url,data=body,headers=headers, auth=HTTPBasicAuth(os.environ['EDHLogin'], os.environ['EDHPasswd']), verify = False)

        root = etree.fromstring(response.content)
        documentResponse = root.xpath("/soapenv:Envelope/soapenv:Body/ns:getDocumentStatusResponse/ns:return",
                        namespaces={'soapenv': 'http://schemas.xmlsoap.org/soap/envelope/', 'ns': 'http://WS.edh.cern'})

        responseCode = documentResponse[0].text
        responseroot = etree.fromstring( responseCode.replace('<?xml version="1.0" encoding="ISO-8859-1"?>','') )

        return responseroot[1].text

    def createTID(self, title, supplierPID, clientPID, clientBudgetCode, parts):

        #Create part's XML:
        itemsXML = ''

        for p in parts:
            print(p)
            itemsXML += '''
                <tid:lineitem>
                    <tid:shortdescription>{}</tid:shortdescription>
                    <tid:longdescription>{}</tid:longdescription>
                    <tid:quantity>{}</tid:quantity>
                    <tid:price>{}</tid:price>
                    <tid:clientbco>
                        <tid:budgetcode>{}</tid:budgetcode>
                    </tid:clientbco>
                    <tid:supplierbco>
                        <tid:budgetcode>{}</tid:budgetcode>
                    </tid:supplierbco>
                    <tid:procurementcode><pc:code>{}</pc:code></tid:procurementcode>
                    <tid:removefrominventory>false</tid:removefrominventory>
                    <tid:enterintoinventory>false</tid:enterintoinventory>
                    <tid:alreadydelivered>false</tid:alreadydelivered>
                    <tid:lifetimeoveroneyear>true</tid:lifetimeoveroneyear>
                </tid:lineitem>
                '''.format(p['short_desc'], p['long_desc'], p['qty'], p['unitprice'], clientBudgetCode, p['supplierBudgetCode'], p['proccode'])

        print(itemsXML)

        tidXML = '''
            <document xmlns="http://edh.cern.ch/2008/EDHDocument" xmlns:tid="http://edh.cern.ch/2008/TID" xmlns:person="http://edh.cern.ch/2006/Person" xmlns:pc="http://edh.cern.ch/2016/ProcurementCode" xmlns:act="http://edh.cern.ch/2006/ActivityCode" xmlns:loc="http://edh.cern.ch/2006/Location" xmlns:contract="http://edh.cern.ch/2008/Contract" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://edh.cern.ch/2008/EDHDocument ../../java/cern/edh/WS/schema/EDHDocument-WS.xsd http://edh.cern.ch/2008/TID ../../java/cern/edh/WS/schema/TID-WS.xsd">
                <common>
                    <type>EDH/TID</type>
                    <description>{}</description>
                    <creator>
                        <person:pid>{}</person:pid>
                    </creator>
                </common>
                <docspecific>
                    <tid:tid>
                        <tid:createdas>supplier</tid:createdas>
                        <tid:client>
                            <person:pid>{}</person:pid>
                        </tid:client>
                        <tid:supplier>
                            <person:pid>{}</person:pid>
                        </tid:supplier>
                        <tid:comment/>
                        <tid:lineitems>
                            {}
                        </tid:lineitems>
                    </tid:tid>
                </docspecific>
            </document>
        '''.format(title, supplierPID, clientPID, supplierPID, itemsXML)

        print(tidXML)

        importXML = '''
            <import-info xmlns="http://edh.cern.ch/2008/EDHDocument"
                xmlns:bc="http://edh.cern.ch/2008/BudgetCode"
                xmlns:person="http://edh.cern.ch/2006/Person"
                xmlns:unit="http://edh.cern.ch/2006/Unit"
                xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
                xsi:schemaLocation="http://edh.cern.ch/2008/EDHDocument../../document-import-ws/src/java/cern/edh/WS/schema/EDHDocument-WS.xsd">
                    <sendDocument>true</sendDocument>
                    <msglines>
                        <msgline><msg>This document has been created automatically by the CERN-DMS webapplication for EP-ESE-ME</msg></msgline>
                    </msglines>
            </import-info>
        '''

        session = Session()
        session.auth = HTTPBasicAuth(os.environ['EDHLogin'], os.environ['EDHPasswd'])
        session.verify = False
        transport_with_basic_auth = Transport(session=session)

        print("Creating client")    
        client = Client(
            wsdl='http://edhws.cern.ch/ws/services/EDHDocumentImport?wsdl',
            transport=transport_with_basic_auth
        )

        print("Creating message")
        node = client.create_message(client.service, 'importDocument', docXml = tidXML, importInfoXml = importXML)

        url="https://edhws.cern.ch/ws/services/EDHDocumentImport"
        headers = {'content-type': 'text/xml'}
        body = etree.tostring(node, pretty_print=True)
        print(node)
        print(body)


        #
        response = requests.post(url,data=body,headers=headers, auth=HTTPBasicAuth(os.environ['EDHLogin'], os.environ['EDHPasswd']), verify = False)

        root = etree.fromstring(response.content)
        responseCode = root.xpath("/soapenv:Envelope/soapenv:Body/ns:importDocumentResponse/ns:return",
                          namespaces={'soapenv': 'http://schemas.xmlsoap.org/soap/envelope/', 'ns': 'http://WS.edh.cern'})[0].text

        code, documentNumber = responseCode.split(':',1)
        print( "Response code", code)
        print( responseCode )
    
        if code == 'OK':
            print( "Document number" ,documentNumber)
            return documentNumber
        else:
            raise Exception('Error: ' + ' ' + responseCode)

        #
        # try:
        #    root = etree.fromstring(response.content)
        #    responseCode = root.xpath("/soapenv:Envelope/soapenv:Body/ns:importDocumentResponse/ns:return",
                                    #    namespaces={'soapenv': 'http://schemas.xmlsoap.org/soap/envelope/', 'ns': 'http://WS.edh.cern'})[0].text
        #
        #    code, tidNumber = responseCode.split(':')
        #
        #    if code == 'OK':
            #    return tidNumber
        # except:
        #    print(response.content)
        #
        #return -1