import lib.diststatus as diststatus
import lib.database as DB

import pprint
import json
import datetime

class Acknowledged(diststatus.Status):
    '''
        # Acknowledged status

        The acknowledged status checks whether all of the devices have been associated to batch or not!
        Once they have been successfully associated, the status can be changed to the Acknowledged one.
        If one of the device is not ready, the update will fail.
    '''

    #Status details
    __name__    = '[CORE] Order acknowledgement'
    __author__  = 'Julian Maxime Mendez'
    __version__ = 'v.1.0'

    def _create_remaining_order(self, remaining_parts):
        #Get order inputs
        user_id = self.order_details['ordered_by_id']
        order_status = self.order_details['curr_status']['status_id']
        order_nextstatus = self.order_details['next_status']['status_id']
        order_date = self.order_details['order_date']
        order_discnt = self.order_details['discount']
        order_details = self.order_details['details']
        order_shipment = self.order_details['shipment']
        order_tobeshipped = self.order_details['tobeshipped']

        sql_request = 'INSERT INTO PurchaseRequest(user_id, status, next_status, order_date, discount, details, shipment, tobeshipped) VALUES(%s,%s, %s, %s, %s,%s,%s,%s) RETURNING purchaserequest_id'
        sql_params = [
            int(user_id),
            int(order_status),
            int(order_nextstatus),
            order_date,
            order_discnt,
            json.dumps(order_details),
            json.dumps(order_shipment),
            order_tobeshipped
        ]

        df = DB.db_get(sql_request, sql_params, connection_details=self.connection_details)
        purchaserequest_id = int(df['purchaserequest_id'][0])

        for part in remaining_parts:
            sql_request = 'INSERT INTO PartPreqLink(part_id, purchaserequest_id, ori_quantity, act_quantity, user_inputs) VALUES(%s,%s,%s,%s,%s)'
            sql_params = [
                int(part['part_id']),
                purchaserequest_id,
                int(part['ori_quantity']),
                int(part['remain_quantity']),
                json.dumps(part['user_inputs'])
            ]

            DB.db_set(sql_request, sql_params, connection_details=self.connection_details)

            if int(part['ack_quantity']) == 0:
                sql_request = 'DELETE FROM PartPreqLink WHERE partpreqlink_id = %s'
                sql_params = [
                    int(part['partpreq_id'])
                ]

                DB.db_set(sql_request, sql_params, connection_details=self.connection_details)

            else:
                sql_request = 'UPDATE PartPreqLink SET act_quantity = %s WHERE partpreqlink_id = %s'
                sql_params = [
                    int(part['ack_quantity']),
                    int(part['partpreq_id'])
                ]

                DB.db_set(sql_request, sql_params, connection_details=self.connection_details)

        return json.dumps({'status': 0})

    def call(self, inputs):
        '''
            Acknowledging a request means that all parts have been associated to batches and are
            ready to be shipped.

            Therefore, before aprouving the "Acknowledged" status, the call function checks that
            all of the parts are linked to a production.
        '''
        remaining_parts = []
        total_ack_qty = 0

        if inputs['Check for stock'] == True:

            for p in self.order_details['parts']:
                ack_nb = int(inputs[p['part_name']])
                if ack_nb == 0:
                    continue

                if inputs['Batch for {}'.format(p['part_name'])] is None:
                    raise Exception('No batch selected for {}'.format(p['part_name']))

                batch_id = int(inputs['Batch for {}'.format(p['part_name'])])

                sqlreq = '''
                    SELECT
                        Batches.quantity as totalqty
                    FROM Batches
                    WHERE Batches.batch_id = %s
                '''
                df_batch = DB.db_get(sqlreq, [batch_id], connection_details=self.connection_details)

                if len(df_batch) <= 0:
                    raise Exception('{} - Batch {} not found in DB'.format(p['part_name'], batch_id))

                batch_qty = int(df_batch['totalqty'][0])

                sqlreq = '''
                    SELECT
                        sum(PurchaseMgt.quantity) as soldqty
                    FROM PartPreqLink, PurchaseMgt, PurchaseRequest
                    WHERE PurchaseMgt.batch_id = %s and
                         PartPreqLink.partpreqlink_id = PurchaseMgt.partpreqlink_id and
                         PurchaseRequest.status >= 0 and
                         PurchaseRequest.purchaserequest_id = PartPreqLink.purchaserequest_id
                '''
                df_pmgt = DB.db_get(sqlreq, [batch_id], connection_details=self.connection_details)

                if len(df_pmgt) <= 0:
                    sold_qty = 0
                else:
                    sold_qty = int(df_pmgt['soldqty'][0])

                remains = batch_qty - sold_qty

                if ack_nb > remains:
                    raise Exception('{} - Batch {} does not contain enough devices'.format(p['part_name'], batch_id))

        for p in self.order_details['parts']:

            ack_nb = int(inputs[p['part_name']])

            if ack_nb < 0:
                raise Exception('{} - Ack number CANNOT be lower than 0'.format(p['part_name']))

            if ack_nb == 0:
                sql_request = 'DELETE FROM PartPreqLink WHERE partpreqlink_id = %s'
                sql_params = [
                    int(p['partpreq_id'])
                ]

                DB.db_set(sql_request, sql_params, connection_details=self.connection_details)

                continue

            remains = p['act_quantity'] - ack_nb
            total_ack_qty += ack_nb
            if remains > 0:
                remaining_parts.append(
                    {
                        'part_id': p['id'],
                        'user_inputs': p['userinputs'],
                        'ack_quantity': ack_nb,
                        'ori_quantity': p['ori_quantity'],
                        'remain_quantity': remains,
                        'partpreq_id': p['partpreq_id']
                    })

            elif remains < 0:
                raise Exception('{} - Ack number shall be lower than {}'.format(p['part_name'], p['act_quantity']))

        if total_ack_qty <= 0:
            raise Exception('Nothing has been acknowledged')

        if len(remaining_parts) > 0:
            #pprint.pprint(remaining_parts)
            self._create_remaining_order(remaining_parts)

        for part in self.order_details['parts']:
            ack_nb = int(inputs[part['part_name']])
            batch_id = int(inputs['Batch for {}'.format(part['part_name'])])

            sql_request = 'INSERT INTO PurchaseMgt(partpreqlink_id, batch_id, status, next_status, quantity, discount, details) VALUES(%s,%s,0,0,%s,0,%s)'
            sql_params = [
                part['partpreq_id'],
                batch_id,
                ack_nb,
                json.dumps({})
            ]

            DB.db_set(sql_request, sql_params, connection_details=self.connection_details)

        self.add_order_details({
            'Acknowledged by': self.user['email'],
            'Acknowledged date': datetime.datetime.now().strftime("%m/%d/%Y, %H:%M:%S")
        }, True)

        if (self.status_id+1) in self.status_list:
            return self.status_id+1
        else:
            return 0 #Last status - Go to done

    def preset(self):
        '''
            An option allows skiping the checking.
        '''
        ret = []

        #pprint.pprint(self.order_details)

        for part in self.order_details['parts']:

            sqlreq = '''
                SELECT
                    Batches.batch_id as batch_id,
                    Batches.name,
                    Batches.quantity,
                    Batches.details
                FROM Batches WHERE Batches.part_id = %s
            '''

            df_batches = DB.db_get(sqlreq, [part['id']], connection_details=self.connection_details)
            batches = df_batches.to_dict(orient='record')

            options = []

            for batch in batches:
                sqlreq = '''
                    SELECT
                        sum(PurchaseMgt.quantity) as soldqty
                    FROM PartPreqLink, PurchaseMgt, PurchaseRequest
                    WHERE PurchaseMgt.batch_id = %s and
                         PartPreqLink.partpreqlink_id = PurchaseMgt.partpreqlink_id and
                         PurchaseRequest.status >= 0 and
                         PurchaseRequest.purchaserequest_id = PartPreqLink.purchaserequest_id
                '''
                df_pmgt = DB.db_get(sqlreq, [batch['batch_id']], connection_details=self.connection_details)

                if len(df_pmgt) <= 0:
                    batch['sold_qty'] = 0
                else:
                    batch['sold_qty'] = int(df_pmgt['soldqty'][0])

                options.append({
                    'value': batch['batch_id'],
                    'text': '{} [{} available]'.format(batch['name'], (batch['quantity']-batch['sold_qty']))
                })

            if len(options) <= 0:
                options.append({
                    'value': -1,
                    'text': 'No batch have been produced'
                })

            ret.append({
                'type': 'text',
                'name': part['part_name'],
                'desc': 'Select quantity to be acknowledged - max: {}'.format(part['act_quantity']),
                'default': part['act_quantity'],
                'options': []
            })
            ret.append({
                'type': 'list',
                'name': 'Batch for {}'.format(part['part_name']),
                'desc': 'Select a batch',
                'default': options[0],
                'options': options
            })

        ret.append({
            'type': 'boolean',
            'name': 'Check for stock',
            'desc': 'Check whether the batch contains enough non-sold devices to fullfill the request',
            'default': True,
            'options': []
        })

        return ret

    def display(self):
        '''
            The display function shows the purchase status for each part.
            It will allow to know whether all of the parts are ready or not.
        '''
        return {}

    def check(self):
        return True