from requests.auth import HTTPBasicAuth  # or HTTPDigestAuth, or OAuth1, etc.
from requests import Session
from zeep import Client
from zeep.transports import Transport
from lxml import etree
import pprint,json
from datetime import date, timedelta

from urllib import parse
from jinja2 import Template

import requests
import os

import lib.diststatus as diststatus

class GenerateSHIP2(diststatus.Status):
    '''
        # SHIP2 Generator

        The acknowledged status checks whether all of the devices have been associated to batch or not!
        Once they have been successfully associated, the status can be changed to the Acknowledged one.
        If one of the device is not ready, the update will fail.
    '''

    #Status details
    __name__    = '[CORE] SHIP2 Generator'
    __author__  = 'Irene Mateos Domínguez'
    __version__ = 'v.1.0'

    #Call function
    def call(self, inputs):
        '''
            Acknowledging a request means that all parts have been associated to batches and are
            ready to be shipped.

            Therefore, before aprouving the "Acknowledged" status, the call function checks that
            all of the parts are linked to a production.
        '''
        if inputs['Skip SHIP2 generation'] == False:
            documentReference = self.createSHIP2(inputs)
            self.add_order_details( {"SHIP2" :documentReference })
            return self.status_id+1 #Next status should be Store shipping request reference

        if "TID" in self.order_details["details"]
            if "TID status" not in self.order_details["details"]:
                self.add_order_details( {"TID status": self.recoverDocumentStatus(self.order_details["details"]["TID"]) } )
            else:
                self.order_details["details"]["TID status"] = self.recoverDocumentStatus(self.order_details["details"]["TID"])
        
        if "SHIP2" in self.order_details["details"]:
            if "SHIP2 status" not in self.order_details["details"]:
                self.add_order_details( {"SHIP2 status": self.recoverDocumentStatus(self.order_details["details"]["SHIP2"]) } )
            else:
                self.order_details["details"]["SHIP2 status"] = self.recoverDocumentStatus(self.order_details["details"]["SHIP2"])

        return 0 # If the SHIP2 generation is not skipped, the flow is finalized and we need to go to 'done'

    #Preset function
    def preset(self):
        '''
            An option allows skiping the checking.
        '''
        print(json.dumps(self.order_details))

        return [
            {
                'type': 'boolean',
                'name': 'Skip SHIP2 generation',
                'desc': 'Check to skip the generation of the SHIP2 document',
                'default': False,
                'options': []
            },
            { 
            'type': 'text',
            'name': f'Description',
            'desc': f'Description for SHIPPING document',
            'default': '',
            'options': [] }
            ]

    #Display function
    def display(self):
        '''
            The display function shows the details of the SHIP2 document that was created.
        '''
        if "SHIP2" in self.order_details["details"]:
            if "SHIP2 status" not in self.order_details["details"]:
                self.add_order_details( {"SHIP2 status": self.recoverDocumentStatus(self.order_details["details"]["SHIP2"]) } )
            else:
                self.order_details["details"]["SHIP2 status"] = self.recoverDocumentStatus(self.order_details["details"]["SHIP2"])

            return {
                "SHIP2 created for this order" : f'<a href=\"http://edh.cern.ch/Document/{self.order_details["details"]["SHIP2"]}\">http://edh.cern.ch/Document/{self.order_details["details"]["SHIP2"]}</a>',
                "SHIP2 status": self.order_details["details"]["SHIP2 status"]
            }
        else:
            return { "No document created for this order.":  "Please create it manually and store the reference." }

    #Check function
    def check(self):
        return True

    def recoverDocumentStatus(self, docId):
        """
            Uses EDH Web Services to recover the state of a document with id docId.
        """
        session = Session()
        session.auth = HTTPBasicAuth(os.environ['EDHLogin'], os.environ['EDHPasswd'])
        session.verify = False
        transport_with_basic_auth = Transport(session=session)
        client = Client(
            wsdl='http://devedhws.cern.ch/ws/services/EDHDocumentImport?wsdl',
            transport=transport_with_basic_auth
        )
        node = client.create_message(client.service, 'getDocumentStatus', docId = 9191959)

        url="https://devedhws.cern.ch/ws/services/EDHDocumentImport"
        headers = {'content-type': 'text/xml'}
        body = etree.tostring(node, pretty_print=True)
        response = requests.post(url,data=body,headers=headers, auth=HTTPBasicAuth(os.environ['EDHLogin'], os.environ['EDHPasswd']), verify = False)

        root = etree.fromstring(response.content)
        documentResponse = root.xpath("/soapenv:Envelope/soapenv:Body/ns:getDocumentStatusResponse/ns:return",
                        namespaces={'soapenv': 'http://schemas.xmlsoap.org/soap/envelope/', 'ns': 'http://WS.edh.cern'})

        responseCode = documentResponse[0].text
        responseroot = etree.fromstring( responseCode.replace('<?xml version="1.0" encoding="ISO-8859-1"?>','') )

        return responseroot[1].text

    def createSHIP2(self, inputs):
        print("Create SHIP2")

        if "Shipping Duration" in self.order_details["shipment"]:
            if self.order_details["shipment"]["Shipping Duration"] == "Temporary":
                shipment_type = 0
            elif self.order_details["shipment"]["Shipping Duration"] == "Permanent":
                shipment_type = 1
            else:
                shipment_type = 2

        #SHIP2 XML structure:
        templateSHIP2 = '''<?xml version="1.0" encoding="ISO-8859-1"?>
<document xmlns="http://edh.cern.ch/2008/EDHDocument"
        xmlns:ship="http://edh.cern.ch/2011/SHIP"  
        xmlns:person="http://edh.cern.ch/2006/Person"
        xmlns:sup="http://edh.cern.ch/2008/Supplier"   
        xmlns:loc="http://edh.cern.ch/2006/Location"
        xmlns:unit="http://edh.cern.ch/2006/Unit"
        xmlns:bc="http://edh.cern.ch/2008/BudgetCode"
        xmlns:cur="http://edh.cern.ch/2006/Currency"
        xmlns:coun="http://edh.cern.ch/2006/Country"	
        xmlns:addr="http://edh.cern.ch/2011/Address"
        xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" 		
xsi:schemaLocation="http://edh.cern.ch/2008/EDHDocument ../../java/cern/edh/WS/schema/EDHDocument-WS.xsd http://edh.cern.ch/2011/SHIP ../../java/cern/edh/WS/schema/SHIP-WS.xsd">
    <common>
        <type>EDH/SHIP</type>
        {% if description.length!= 0%}
        <description>{{description}}</description>
        {% endif %}
        <creator>
            <person:pid>{{admin_pid}}</person:pid>
        </creator>
    </common>
    <docspecific>
    <ship:ship>
        <ship:shippingDocumentType>0</ship:shippingDocumentType> <!-- Shipping Request (Expedition) -->
        <ship:generalDescription>asic_distrib_{{user_lastname}}_TID#{{TID_number}}</ship:generalDescription>
        <ship:ownerType>0</ship:ownerType> <!-- CERN           -->
        <ship:ownerUnit>
            <unit:name>EP-ESE-ME</unit:name> <!-- Change to ME -->
        </ship:ownerUnit>
        <ship:pickupContact>
            <person:pid>851539</person:pid> <!-- 851539 -->
        </ship:pickupContact>
        <ship:transportHazard>0</ship:transportHazard> <!-- Non Hazardous Material          -->
        <ship:pickupPoint> <!-- 14-6-023 BUREAU MEYRIN  -->
            <loc:bld>14</loc:bld>
            <loc:floor>6</loc:floor>
            <loc:room>023</loc:room>
        </ship:pickupPoint>
        <ship:transportCollect>1</ship:transportCollect> <!-- Prevessin  -->
        <!-- <ship:destType>1</ship:destType> --> <!-- DESTINATION_MANUAL Enter Address by Hand --> 
        <!-- <ship:destinationManual>
            <addr:name>{{shipment["company_institute"]}}</addr:name>
            <addr:address1>{{shipment["address_1"]}}</addr:address1>
            <addr:address2>{{shipment["address_2"]}}</addr:address2>
            <addr:postCode>{{shipment["postcode"]}}</addr:postCode>
            <addr:city>{{shipment["city"]}}</addr:city>			
            <addr:country>
                <coun:code>{{shipping_country_code}}</coun:code>
            </addr:country>
            <addr:tel>{{shipment["contact_telephone"]}}</addr:tel>			
            <addr:email>{{shipment["contact_email"]}}</addr:email>
        </ship:destinationManual> -->
        <ship:destination>
            <sup:code>
                CERN
            </sup:code>
            <sup:addrCode>
                EL
            </sup:addrCode>
        </ship:destination>
        <ship:attentionOf>{{user_name}} {{user_lastname}}</ship:attentionOf>
        <ship:shippingType>{{shipment_type}}</ship:shippingType> <!-- 0 SHIPTYPE_TEMPORARY  --> <!-- 1 SHIPTYPE_PERMANENT  -->
        {% if shipment_type == 0 %}
            <ship:returnDate>{{current_date_plus2y}}</ship:returnDate>
        {% endif %}
        <ship:reason>0</ship:reason> <!-- REASON_ORDER          -->
        <ship:chargesPaidBy>2</ship:chargesPaidBy> <!-- SHIPCOSTS_TIER  --> <!-- Paid by recipient (?) -->
        <ship:budgetCode>{{budget_code}}</ship:budgetCode>
        <ship:currency>
            <cur:code>CHF</cur:code>
        </ship:currency>
        <!-- Items start -->
        <ship:lineitems>
            {% for part in parts %}
            <!-- Single item starts -->
            <ship:lineitem>
                <ship:shortDescription>{{part["part_name"]}}</ship:shortDescription>
                <ship:longDescription>Electronic components for detector</ship:longDescription>
                <ship:quantity>{{part["act_quantity"]}}</ship:quantity>
                <ship:objectType>PC</ship:objectType> <!-- Piece          -->				
                <ship:unitValue>{{part["unit_price"]}}</ship:unitValue>
                <ship:country>
                    <coun:code>{{part["Country of origin (code)"]}}</coun:code>	<!-- part.country_of_origin -->			
                </ship:country>
            </ship:lineitem>
            {% endfor %}
            <!-- Single item ends -->
        </ship:lineitems>
        <!-- Items end -->
        <ship:transportRequired>2</ship:transportRequired> <!-- TRANSPORT_REQUIRED_AND_INTERNAL      -->
        <ship:transportFragile>0</ship:transportFragile> <!-- TRANSPORT_NOT_FRAGILE  -->	
        <ship:transportPack>0</ship:transportPack>	<!-- TRANSPORT_ALREADY_PACKAGED               -->	
        <ship:numberPackages>1</ship:numberPackages>		
        <ship:colissage>CN</ship:colissage>	<!-- CARTON -->
        <ship:totalWeight>0</ship:totalWeight>		
        <ship:length>0</ship:length>		
        <ship:width>0</ship:width>		
        <ship:height>0</ship:height>
        <ship:latestPickupDate>{{pickup_date}}</ship:latestPickupDate>		<!-- ASK - current date plus a week? -->
        <!-- <ship:latestDeliveryDate>2011-12-22</ship:latestDeliveryDate> --> <!-- ASK --> <!-- Not mandatory -->
        <!-- <ship:transportInstructions> </ship:transportInstructions> -->	<!-- Not mandatory -->
        <ship:commentsConsignee>
        {% if shipping_in_eu %}
            "Temporary and free circulation of dual-use goods within the EU borders with final destination CERN Prévessin (FR) for the official use of the Organisation."
        {% endif %}
        </ship:commentsConsignee>		
        <!-- <ship:commentsOther></ship:commentsOther> --> <!-- Not mandatory --> 					
    </ship:ship>
    </docspecific>
</document>
        '''

        countries_in_eu = ["AT", "BE", "BG", "HR", "CY", "CZ", "DK", "EE", "FI", "FR", "DE", "GR", "HU", "IE", "IT", "LV", "LT", "LU", "MT", "NL", "PL", "PT", "RO", "SK", "SI", "ES", "SE"]

        # Prepare data to fill the template
        pickup_date = date.today() + timedelta(days=7)
        current_date_plus2y = date.today() + timedelta(days=2*365)
        # print(f'https://restcountries.com/v3.1/name/{self.order_details["shipment"]["Country"]}')
        # print( requests.get(f'https://restcountries.com/v3.1/name/{self.order_details["shipment"]["Country"]}').json()[0]["cca2"] )
        shipping_country_code = requests.get(f'https://restcountries.com/v3.1/name/{ parse.quote(self.order_details["shipment"]["Country"]) }').json()[0]["cca2"]
        admin_pid = 594354 # David 594354 #TODO read from DB or env vars
        description = inputs['Description']

        if "TID" in self.order_details["details"]:
            TID_number = self.order_details["details"]["TID"]
        else:
            TID_number = "X"

        parts = self.order_details["parts"].copy()
        # Country of origin and unit price
        for part in parts:
            for detail in part["part_details"]:
                if detail["name"] == "Country of origin (code)":
                    part["Country of origin (code)"] = detail["value"]

            for index in range(len(part["part_unit_prices"])):
                price = part["part_unit_prices"][index]
                print(price)
                print(index)
                if price["qty"] < part["act_quantity"]:
                    if index < len(part["part_unit_prices"])-1:
                        if part["part_unit_prices"][index+1]["qty"] > part["act_quantity"]:
                            part["unit_price"] = price["chf"]
                    else:
                        part["unit_price"] = price["chf"]
                    
        print("Rendering..")

        # Render XML
        t = Template(templateSHIP2)
        renderedSHIP2_XML = t.render(  parts = self.order_details["parts"],  \
            user_name = self.order_details["ordered_by_details"]["given_name"], \
            user_lastname = self.order_details["ordered_by_details"]["family_name"], \
            current_date_plus2y = current_date_plus2y, \
            shipment = self.order_details["shipment"], \
            shipment_type = shipment_type, \
            budget_code = self.order_details["details"]["Budget Code"], \
            TID_number =  TID_number, \
            admin_pid = admin_pid, \
            shipping_country_code = shipping_country_code , \
            pickup_date = pickup_date, \
            description = description, \
            shipping_in_eu = shipping_country_code in countries_in_eu)

        # print("Printing rendered value")

        # print(renderedSHIP2_XML)
        # with open('renderedSHIP2_XML.xml', 'w') as f:
        #     f.write(renderedSHIP2_XML)

        # return

        importXML = '''
            <import-info xmlns="http://edh.cern.ch/2008/EDHDocument"
                xmlns:bc="http://edh.cern.ch/2008/BudgetCode"
                xmlns:person="http://edh.cern.ch/2006/Person"
                xmlns:unit="http://edh.cern.ch/2006/Unit"
                xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
                xsi:schemaLocation="http://edh.cern.ch/2008/EDHDocument../../document-import-ws/src/java/cern/edh/WS/schema/EDHDocument-WS.xsd">
                    <sendDocument>true</sendDocument>
                    <msglines>
                        <msgline><msg>This document has been created automatically by the CERN-DMS webapplication for EP-ESE-ME</msg></msgline>
                    </msglines>
            </import-info>
        '''

        session = Session()
        session.auth = HTTPBasicAuth(os.environ['EDHLogin'], os.environ['EDHPasswd'])
        session.verify = False
        transport_with_basic_auth = Transport(session=session)

        # print("Creating client")    
        client = Client(
            wsdl='http://edhws.cern.ch/ws/services/EDHDocumentImport?wsdl',
            transport=transport_with_basic_auth
        )

        # print("Creating message")
        node = client.create_message(client.service, 'importDocument', docXml = renderedSHIP2_XML, importInfoXml = importXML)

        url="https://edhws.cern.ch/ws/services/EDHDocumentImport"
        headers = {'content-type': 'text/xml'}
        body = etree.tostring(node, pretty_print=True)
        print("NODE: ", node)
        print("BODY: ", body)

        
        #
        response = requests.post(url,data=body,headers=headers, auth=HTTPBasicAuth(os.environ['EDHLogin'], os.environ['EDHPasswd']), verify = False)
        #
        root = etree.fromstring(response.content)
        responseCode = root.xpath("/soapenv:Envelope/soapenv:Body/ns:importDocumentResponse/ns:return",
                          namespaces={'soapenv': 'http://schemas.xmlsoap.org/soap/envelope/', 'ns': 'http://WS.edh.cern'})[0].text
        code, documentNumber = responseCode.split(':',1)
        print( "Response code", code)
        print( responseCode )
    
        if code == 'OK':
            print( "Document number" ,documentNumber)
            return documentNumber
        else:
            raise Exception('Error: ' + ' ' + responseCode)
