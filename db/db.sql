DROP TABLE IF EXISTS Users;
DROP TABLE IF EXISTS Categories;
DROP TABLE IF EXISTS Parts;
DROP TABLE IF EXISTS PartCatLink;
DROP TABLE IF EXISTS Packages;
DROP TABLE IF EXISTS PackPartLink;
DROP TABLE IF EXISTS PackCatLink;
DROP TABLE IF EXISTS Batches;
DROP TABLE IF EXISTS Devices;
DROP TABLE IF EXISTS DeviceUserLink;
DROP TABLE IF EXISTS UserShipmentAddr;
DROP TABLE IF EXISTS PurchaseRequest;
DROP TABLE IF EXISTS PurchaseMgt;
DROP TABLE IF EXISTS PartPreqLink;
DROP TABLE IF EXISTS PackPreqLink;
DROP TABLE IF EXISTS PackPreqPartLink;

/* ************************************************************ */
/* SYSTEM MANAGEMENT DATABASE                                   */
/* ************************************************************ */
CREATE TABLE FileIDs(
    fileIds_id          SERIAL          PRIMARY KEY ,
    sha_1               VARCHAR(256)                ,
    section_name        VARCHAR(128)
);

/* ************************************************************ */
/* USER DATABASE                                                */
/* ************************************************************ */
CREATE TABLE Users(
    user_id             SERIAL          PRIMARY KEY ,
    email_addr          VARCHAR(256)                ,
    details             JSONB
);

/* ************************************************************ */
/* DISTRIBUTION MODULE DATABASE                                 */
/* ************************************************************ */

CREATE TABLE Categories(
    category_id         SERIAL          PRIMARY KEY ,
    name                VARCHAR(256)                ,
    description         TEXT                        ,
    mandatory_details  JSONB
);

CREATE TABLE Parts(
    part_id             SERIAL          PRIMARY KEY ,
    name                VARCHAR(256)                ,
    description         TEXT                        ,
    available           BOOLEAN                     ,
    reqshipment         BOOLEAN                     ,
    moq                 INTEGER                     ,
    details             JSONB                       ,
    unit_prices         JSONB                       ,
    user_inputs         JSONB                       ,
    batch_req_details   JSONB                       ,
    purchase_status     JSONB
);

CREATE TABLE PartAdminLink(
    partadminlink_id    SERIAL          PRIMARY KEY ,
    user_id             BIGINT                      ,
    part_id             BIGINT
);

CREATE TABLE PartCatLink(
    partcatlink_id      SERIAL          PRIMARY KEY ,
    part_id             BIGINT                      ,
    category_id         BIGINT
);

CREATE TABLE Packages(
    package_id          SERIAL          PRIMARY KEY ,
    name                VARCHAR(256)                ,
    description         TEXT                        ,
    available           BOOLEAN                     ,
    user_inputs         JSONB                       ,
    details             JSONB
);

CREATE TABLE PackPartLink(
    packpartlink_id     SERIAL          PRIMARY KEY ,
    package_id          BIGINT                      ,
    part_id             BIGINT                      ,
    quantity            INT                         ,
    fixed_inputs        JSONB                       ,
    from_package_inputs JSONB
);

CREATE TABLE PackCatLink(
    packcatlink_id      SERIAL          PRIMARY KEY ,
    package_id          BIGINT                      ,
    category_id         BIGINT
);

CREATE TABLE Batches(
    batch_id            SERIAL          PRIMARY KEY ,
    part_id             BIGINT                      ,
    name                VARCHAR(256)                ,
    planned_date        TIMESTAMP                   ,
    actual_date         TIMESTAMP                   ,
    quantity            BIGINT                      ,
    expected_yield      FLOAT                       ,
    details             JSONB
);

CREATE TABLE Devices(
    device_id           SERIAL          PRIMARY KEY ,
    name                VARCHAR(512)                ,
    batch_id            BIGINT                      ,
    current_status      INT                         ,
    afterprod_status    INT                         ,
    details             JSONB
);

CREATE TABLE DeviceUserLink(
    deviceuserlink_id   SERIAL          PRIMARY KEY ,
    device_id           BIGINT                      ,
    user_id             BIGINT
);

CREATE TABLE OrderConfig(
    orderconfig_id      SERIAL          PRIMARY KEY ,
    order_userinputs    JSONB                       ,
    shipment_userinputs JSONB                       ,
    purchase_status     JSONB
);

-- a purchase request can be subdivised. In that case,
-- status becomes -1 (cancelled) and
CREATE TABLE PurchaseRequest(
    purchaserequest_id  SERIAL          PRIMARY KEY ,
    user_id             BIGINT                      ,
    status              INT                         ,
    next_status         INT                         ,
    order_date          TIMESTAMP                   ,
    discount            FLOAT                       ,
    details             JSONB                       ,
    shipment            JSONB                       ,
    tobeshipped         BOOLEAN
);

CREATE TABLE PartPreqLink(
    partpreqlink_id     SERIAL          PRIMARY KEY ,
    part_id             BIGINT                      ,
    purchaserequest_id  BIGINT                      ,
    act_quantity        BIGINT                      ,
    ori_quantity        BIGINT                      ,
    user_inputs         JSONB
);

CREATE TABLE PurchaseMgt(
    purchasemgt_id      SERIAL          PRIMARY KEY ,
    partpreqlink_id     BIGINT                      ,
    batch_id            BIGINT                      ,
    status              INT                         , -- -1: cancelled / -2: shipped
    next_status         INT                         ,
    quantity            BIGINT                      ,
    discount            FLOAT                       ,
    details             JSONB
);

/* CREATE TABLE PackPreqLink(
    packpreqlink_id     SERIAL          PRIMARY KEY ,
    package_id          BIGINT                      ,
    purchaserequest_id  BIGINT                      ,
    quantity            BIGINT                      ,
    user_inputs         JSONB
);

CREATE TABLE PackPreqPartLink(
    packpreqpartlink_id SERIAL          PRIMARY KEY ,
    packpreqlink_id     BIGINT                      ,
    part_id             BIGINT                      ,
    user_inputs         JSONB
); */

/* ****************************************************************************************** */
/* TEST RESULT DATABASE                                                                       */
/* ****************************************************************************************** */
CREATE TABLE TesterConnectionKeys (
    tester_id     SERIAL          PRIMARY KEY ,
    key_str       VARCHAR(255)                ,
    gen_date      TIMESTAMP                   ,
    enabled       BOOLEAN
);

CREATE TABLE TestInstances (
    test_id     SERIAL          PRIMARY KEY ,
    device_id   BIGINT                      ,
    date        TIMESTAMP                   ,
    details     JSONB                       ,
    summary     JSONB                       ,
    display     JSONB                       ,
    pass        BIGINT
);

CREATE TABLE TestTypes(
    testtype_id SERIAL          PRIMARY KEY ,
    name        VARCHAR(128)                ,
    details     JSONB
);

CREATE TABLE SubTestInstances (
    subtest_id  SERIAL          PRIMARY KEY ,
    test_id     BIGINT                      ,
    testtype_id BIGINT                      ,
    name        VARCHAR(128)                ,
    details     JSONB                       ,
    summary     JSONB                       ,
    pass        BIGINT
);

CREATE TABLE MeasurementInstances(
    meas_id     SERIAL          PRIMARY KEY ,
    test_id     BIGINT                      ,
    testtype_id BIGINT                      ,
    subtest_id  BIGINT                      ,
    device_id   BIGINT                      ,
    name        VARCHAR(128)                ,
    datapoint   JSONB                       ,
    pass        BIGINT
);