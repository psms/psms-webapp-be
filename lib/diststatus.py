import traceback
import os
import json

import lib.database as DB

class Status():

    def __init__(self, user, order_id, order_desc, status_list):
        self.order_id = order_id
        self.order_details = order_desc
        self.status_id = int(order_desc['next_status']['status_id'])
        self.user = user
        self.status_list = status_list

        self.connection_details = {
            'DB_HOST': os.environ['DB_HOST'],
            'DB_USER': os.environ['DB_USER'],
            'DB_PASS': os.environ['DB_PASS'],
            'DB_PORT': os.environ['DB_PORT'],
            'DB_DBNAME': os.environ['DB_DBNAME']
        }

    def _register_status(self):
        return True

    def _call(self, status, inputs):
        try:
            next_status = self.call(inputs)

            df = DB.db_set(
                '''
                    UPDATE PurchaseRequest
                        SET next_status = %s, status = %s
                    WHERE purchaserequest_id = %s
                ''', [int(next_status), status, self.order_id], connection_details=self.connection_details)
        except Exception as e:
            return {'status': -2, 'errors': [
                    {'msg': str(e), 'desc': traceback.format_exc()}
                ]
            }

        return {'status': 0}

    def add_order_details(self, details, force_update=False):
        for k in details:
            if force_update == False and k in self.order_details['details']:
                raise Exception('Key already exists')

            self.order_details['details'][k] = details[k]

        print(self.order_details['details'])

        df = DB.db_set(
            '''
                UPDATE PurchaseRequest
                    SET details = %s
                WHERE purchaserequest_id = %s
            ''', [json.dumps(self.order_details['details']), self.order_id], connection_details=self.connection_details)