# We need to monkey_patch everything
from flask import request
from flask import session
from flask import Response
from flask import make_response
from flask import redirect
from flask_cors import cross_origin
from authlib.integrations.flask_client import OAuth

import json
import base64
import os
import lib.database as DB

class SignON():
    def __init__(self, app):
        oauth = OAuth(app)
        self.oauth_client = oauth.register(
            name='cern-dms',
            client_id=os.environ['SSO_CLIENTID'],
            access_token_url='https://auth.cern.ch/auth/realms/cern/protocol/openid-connect/token',
            access_token_method='POST',
            authorize_url='https://auth.cern.ch/auth/realms/cern/protocol/openid-connect/auth',
            api_base_url='https://auth.cern.ch/auth/realms/cern/protocol/openid-connect/'
        )

        self.connection_details = {
            'DB_HOST': os.environ['DB_HOST'],
            'DB_USER': os.environ['DB_USER'],
            'DB_PASS': os.environ['DB_PASS'],
            'DB_PORT': os.environ['DB_PORT'],
            'DB_DBNAME': os.environ['DB_DBNAME']
        }

    def get_user_details(self):
        if 'token' not in session:
            raise Exception('Not connected')

        token = session['token']
        print('Token', token)
        token_split = token['access_token'].split('.')
        print('Split token', token_split)

        for i in range(4-(len(token_split[1])%4)):
            token_split[1] += '='

        print(json.loads(base64.b64decode(token_split[1])))

        roles = json.loads(base64.b64decode(token_split[1]))['resource_access'][self.oauth_client.client_id]['roles']
        details = self.oauth_client.get('userinfo', token=token).json()

        if 'error' in details:
            raise Exception('Not connected')


        r = DB.db_get(
        '''
            SELECT
                user_id,
                email_addr
            FROM Users
            WHERE email_addr = %s
        ''', [details['email']], connection_details=self.connection_details)

        if len(r) == 0:
            tmp = details
            tmp['username'] = details['cern_upn']

            r = DB.db_get(
            '''
                INSERT INTO Users(email_addr, details) VALUES (%s, %s) RETURNING user_id
            ''', [details['email'],json.dumps(tmp)], connection_details=self.connection_details)

        else:
            tmp = details
            tmp['username'] = details['cern_upn']

            DB.db_set(
            '''
                UPDATE Users SET details = %s WHERE user_id = %s
            ''', [json.dumps(tmp), int(r['user_id'][0])], connection_details=self.connection_details)

        usr_id = int(r['user_id'][0])

        return {
            'user_id': usr_id,
            'email': str(r['email_addr'][0]),
            'details': details,
            'roles': roles
        }

    def get_connection_url(self, redirect):
        r = self.oauth_client.authorize_redirect('{}restapi/user/connected?r={}'.format(request.url_root.replace('http:/','https:/'), redirect))
        return r.headers.get('Location')

    def get_disconnect_url(self, redirect):
        return '{}logout?redirect_uri={}restapi/user/disconnected?r={}'.format(self.oauth_client.api_base_url,request.url_root.replace('http:/','https:/'), redirect)

    def oauth_connect(self):
        token = self.oauth_client.authorize_access_token()
        session['token'] = token
