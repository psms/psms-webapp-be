import psycopg2
from io import StringIO
import pandas
import sys
import os
import json

class DBException(Exception):
    pass

'''
Database Object
Functonality to establish a connection to the database and execute SQL statements
'''
class Database:

    #Constructor
    def __init__(self,host,username,password,port,dbname):
        self.host = host
        self.username = username
        self.password = password
        self.port = port
        self.dbname = dbname

        self.conn = self.dbconnect()
        if(self.conn == -1):
            raise Exception('DB connection error, please check the host, username, password, port and dbname settings.')
        self.cur = self.conn.cursor()
        self.conn.autocommit = True

    ''' Connect to the database '''
    def dbconnect(self):
        try:
            self.conn = psycopg2.connect( host=self.host, port=self.port,user=self.username, password=self.password, dbname=self.dbname )
            return self.conn
        except:
            #database error
            return -1

    ''' Execute SQL with response '''
    def dbquery(self, sqlcommand, params=None, fetchall=True):
        try:
            cursor = self.conn.cursor()
            cursor.execute(sqlcommand, params)
            self.conn.commit()

            if fetchall == True:
                list= cursor.fetchall()
                cursor.close()
                return list
            else:
                return cursor

        except psycopg2.Error as e:
            print('Error: {}'.format(e.pgerror))
            raise ValueError('[DB]: {}'.format(e.pgerror))
            return -2

    def dbfetch(self, sql, args):
        tmp = StringIO()
        sql_command = self.cur.mogrify(sql, args)
        if sys.version_info[0] < 3:
            self.cur.copy_expert("COPY ({}) TO STDOUT DELIMITER ',' CSV HEADER; ".format(str(sql_command)), tmp)
        else:
            self.cur.copy_expert("COPY ({}) TO STDOUT DELIMITER ',' CSV HEADER; ".format(str(sql_command, "utf-8")), tmp)
        tmp.seek(0)
        return pandas.read_csv(tmp, engine='c', index_col=None)

    ''' Execute SQL without response '''
    def dbsend(self, sqlcommand, params):
        #try:
        self.cur.execute( sqlcommand, params)

        #except psycopg2.Error as e:
        #    return -2

    ''' Execute SQL without response '''
    def dbsend_multi(self, sqlcommand, params):
        data = str(tuple('%s' for _ in range(len(params[0])))).replace("'", "")
        args = ','.join(self.cur.mogrify(data, i).decode('utf-8')
                        for i in params)
        self.cur.execute(sqlcommand + (args))

    ''' Disconnect from the database '''
    def dbdisconnect(self):
        self.conn.close()

    def __del__(self):
        if type(self.conn) != int:
            self.dbdisconnect()

def db_set(sql_request, sql_params, connection_details=None):

    if connection_details is None:
        try:
            host = os.environ['DB_HOST']
            username = os.environ['DB_USER']
            password = os.environ['DB_PASS']
            port = os.environ['DB_PORT']
            dbname = os.environ['DB_DBNAME']
        except:
            raise DBException('Connection details not set.')

    else:
        host = connection_details['DB_HOST']
        username = connection_details['DB_USER']
        password = connection_details['DB_PASS']
        port = connection_details['DB_PORT']
        dbname = connection_details['DB_DBNAME']

    inst = Database(host,username,password,port,dbname)

    formatted_params = []
    for p in sql_params:
        if isinstance(p, list):
            formatted_params.append(json.dumps(p))
        else:
            formatted_params.append(p)

    inst.dbsend(sql_request, formatted_params)
    inst.dbdisconnect()

def db_set_multi(sql_request, params, connection_details=None):

    if connection_details is None:
        try:
            host = os.environ['DB_HOST']
            username = os.environ['DB_USER']
            password = os.environ['DB_PASS']
            port = os.environ['DB_PORT']
            dbname = os.environ['DB_DBNAME']
        except:
            raise DBException('Connection details not set.')

    else:
        host = connection_details['DB_HOST']
        username = connection_details['DB_USER']
        password = connection_details['DB_PASS']
        port = connection_details['DB_PORT']
        dbname = connection_details['DB_DBNAME']

    inst = Database(host, username, password, port, dbname)

    inst.dbsend_multi(sql_request, params)
    inst.dbdisconnect()

def db_get(sql_request, sql_params, connection_details=None):

    if connection_details is None:
        try:
            host = os.environ['DB_HOST']
            username = os.environ['DB_USER']
            password = os.environ['DB_PASS']
            port = os.environ['DB_PORT']
            dbname = os.environ['DB_DBNAME']
        except:
            raise DBException('Connection details not set.')

    else:
        host = connection_details['DB_HOST']
        username = connection_details['DB_USER']
        password = connection_details['DB_PASS']
        port = connection_details['DB_PORT']
        dbname = connection_details['DB_DBNAME']

    inst = Database(host,username,password,port,dbname)

    formatted_params = []
    for p in sql_params:
        if str(type(p)) == 'list':
            formatted_params.append(json.dumps(p))
        else:
            formatted_params.append(p)

    v = inst.dbfetch(sql_request, formatted_params)
    inst.dbdisconnect()

    return v

