from smtplib import SMTP, SMTPException

class EmailClient:
    smtpObj = None
    sender = {}

    def __init__(self, server = "smtp.cern.ch" , port = "587"):
        """
            Initializes the connection to the email server. No arguments needed unless the server and port have to be changed.
        """
        self.smtpObj = SMTP(f"{server}:{port}")
        self.smtpObj.starttls()

    def login(self, name, email, password):
        """
            Logins to the sender account.
        """
        self.sender['name'] = name
        self.sender['email'] = email
        self.smtpObj.login(email, password)

    def send_email(self, receivers, subject, message):
        """
            Send email.

            Args:
                receivers: list of dictionaries, one for each receiver:

                [
                    {
                        'name' : <Name of receiver 1>,
                        'email': <Email of receiver 1>
                    },
                    ...
                    {
                        'name' : <Name of receiver 2>,
                        'email': <Email of receiver 2>
                    },
                ]

                subject: Subject for the email.
                message: Body of the email.
        """
        receivers_string = "To: To "
        receivers_emails = []
        for receiver in receivers:
            receivers_string += f" {receiver['name']} <{receiver['email']}>,"
            receivers_emails.append(receiver['email'])
        receivers_string = receivers_string[:-1]
        message_final = f"""From: From {self.sender['name']} <{self.sender['email']}>
To: To {receivers_string}
Subject: {subject}

{message}
        """
        try:
            self.smtpObj.sendmail(self.sender['email'], receivers_emails, message_final)
        except SMTPException:
           print("Error: unable to send email")
           print(SMTPException)
           raise    
