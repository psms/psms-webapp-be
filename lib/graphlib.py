import matplotlib.pyplot as plt
import matplotlib.dates as mdates
import matplotlib.cm as cm
from dateutil.parser import parse
import numbers
import pandas
import numpy as np

class GraphLib:

    def traceHistGraph(self, df, xcol, bins=100, op=None, groups=None, title='No title', filled=False):

        fig, ax = plt.subplots(figsize=(9, 4))
        df.dropna(subset=[xcol], how='all', inplace = True)

        if op is not None:
            if 'op' not in op or op['op'] == 'NONE':
                dfGrp = df.groupby(xcol)

            elif op['op'] == 'ROUND':
                if 'division' not in op:
                    op['division'] = 1

                if len(df) == 0:
                    raise Exception('No data set for {}'.format(xcol))

                df[xcol] = df[xcol].apply(lambda x: round((float(x)/float(op['division'])), int(op['roundVal'])))
                dfGrp = df.groupby(xcol)

            elif op['op'] == 'DATECAST':
                df[xcol] = pandas.to_datetime(df[xcol])
                df[xcol] = pandas.Grouper(key=xcol, freq=op['freq'])

            else:
                raise Exception('Unknown operator: {}'.format(op))

        range_min = df[xcol].min()
        range_max = df[xcol].max()

        if groups is None:
            df['group'] = xcol.replace('_',' ')
            groups = ['group']

        dfSubPlots = df.groupby(groups)

        for name, subdf in dfSubPlots:
            if filled:
                try:
                    computed_bins = int((df[xcol].max()-df[xcol].min())*pow(len(df),(1/3))/(3.49*subdf[xcol].std()))+1
                    if computed_bins < 100:
                        computed_bins = 100
                except:
                    computed_bins = 100

                print('BINS: {}'.format(computed_bins))
                subdf[xcol].hist(bins=computed_bins, range=(range_min, range_max), label=name, density=False)
            else:
                try:
                    computed_bins = int((df[xcol].max()-df[xcol].min())*pow(len(df),(1/3))/(3.49*subdf[xcol].std()))+1
                    if computed_bins < 100:
                        computed_bins = 100
                except:
                    computed_bins = 100
                print('BINS: {}'.format(computed_bins))
                subdf[xcol].hist(bins=computed_bins, range=(range_min, range_max), label=name, histtype=u'step', density=False)

        ax.set_xlabel(xcol)

        return (fig, ax)

    def traceBarGraph(self, df, xcol, ycol, op=None, groups=None, agg='count', title='No title'):

        fig, ax = plt.subplots(figsize=(9, 4))
        df.dropna(subset=[xcol], how='all', inplace = True)

        print(xcol)
        if op is not None:
            if 'op' not in op or op['op'] == 'NONE':
                dfGrp = df.groupby(xcol)

            elif op['op'] == 'ROUND':
                if 'division' not in op:
                    op['division'] = 1

                if len(df) == 0:
                    raise Exception('No data set for {}'.format(xcol))

                df[xcol] = df[xcol].apply(lambda x: round((float(x)/float(op['division'])), int(op['roundVal'])))
                dfGrp = df.groupby(xcol)

            elif op['op'] == 'DATECAST':
                df[xcol] = pandas.to_datetime(df[xcol])
                df[xcol] = pandas.Grouper(key=xcol, freq=op['freq'])
                dfGrp = df.groupby(df[xcol])

            else:
                raise Exception('Unknown operator: {}'.format(op))
        else:
            dfGrp = df.groupby(xcol)

        #for name, df in dfGrp:
        #    print(df)
        dftmp = dfGrp.agg(agg).reset_index().sort_values(by=[xcol])
        OriginalbarDelta = (dftmp[xcol].diff().min())

        if groups is None:
            df['group'] = xcol.replace('_',' ')
            groups = ['group']

        dfSubPlots = df.groupby(groups)
        graph_cnt = len(dfSubPlots)

        graph_id = 0
        for name, df in dfSubPlots:

            if op is not None:
                if 'op' not in op or op['op'] == 'NONE':
                    dfGrp = df.groupby(xcol, as_index=False)

                elif op['op'] == 'ROUND':
                    if 'division' not in op:
                        op['division'] = 1

                    if len(df) == 0:
                        raise Exception('No data set for {}'.format(xcol))

                    df[xcol] = df[xcol].apply(lambda x: round((float(x)/float(op['division'])), int(op['roundVal'])))
                    dfGrp = df.groupby(xcol)

                elif op['op'] == 'DATECAST':
                    df[xcol] = pandas.to_datetime(df[xcol])
                    df[xcol] = pandas.Grouper(key=xcol, freq=op['freq'])
                    dfGrp = df.groupby(df[xcol], as_index=False)

                else:
                    raise Exception('Unknown operator: {}'.format(op))
            else:
                dfGrp = df.groupby(xcol, as_index=False)

            df = dfGrp.agg(agg).reset_index()
            #print(df)

            barDelta = (OriginalbarDelta/graph_cnt)
            width = 0.9*barDelta

            spread = df[xcol].max() - df[xcol].min()
            if spread != 0 and (width/spread) < 1e-3:
                raise Exception('Width vs. spread is too small to be displayed (r={} - spread: {} [min({})/max({})] vs. bar width: {}). Please, try using the round function'.format(
                        (width/spread), spread, df[xcol].min(), df[xcol].max(), width
                    ))

            positions = (df[xcol].values + (graph_id*barDelta) + (0.5*((graph_cnt-1)*barDelta)))
            frequencies = df[ycol].values
            plt.bar(positions, frequencies, width=width, label=name)

            graph_id += 1


        return (fig, ax)

    def traceXYGraph(self, df, xcol, ycol, groups=None, type='circle', title='No Title'):

        print(df)
        print(df[xcol])

        df.dropna(subset=xcol, how='all', inplace = True)

        ''' Generate graph image '''
        y_is_date = False
        try:
            tmp = parse(df[ycol].iloc[0])
            df[ycol] = df[ycol].apply(lambda x: mdates.datestr2num(x))
            y_is_date = True
        except:
            pass

        y_is_text = False
        if isinstance(df[ycol].iloc[0], numbers.Number) == False:
            labels, uniques = df[ycol].factorize()
            df[ycol] = labels
            y_is_text = True

        x_is_date = False
        try:
            tmp = parse(df[xcol].iloc[0])
            df[xcol] = df[xcol].apply(lambda x: mdates.datestr2num(x))
            x_is_date = True
        except:
            pass

        fig, ax = plt.subplots(figsize=(9, 4))

        if groups != None:
            dfgroup = df.groupby(groups)
            colors = cm.rainbow(np.linspace(0, 1, len(dfgroup)))

            if type == 'circle':
                idx = 0
                for name, df in dfgroup:
                    df.plot.scatter(x=xcol, y=ycol, ax=ax, label=name, color=colors[idx])
                    idx += 1
            else:
                idx = 0
                for name, df in dfgroup:
                    df = df.sort_values(by=[xcol])
                    df.plot(x=xcol, y=ycol, ax=ax, label=name)
                    idx += 1

        else:
            if type == 'circle':
                df.plot.scatter(x=xcol, y=ycol, ax=ax)
            else:
                df = df.sort_values(by=[xcol])
                df.plot(x=xcol, y=ycol, ax=ax)

        if y_is_text:
            ax.set_yticks(df[ycol].drop_duplicates().values)
            ax.set_yticklabels(uniques)
        elif y_is_date:
            ax.yaxis_date()

        if x_is_date:
            ax.xaxis_date()
            fig.autofmt_xdate()

        return (fig, ax)
